package edu.bbte.idde.kdim1928.desktop;

import edu.bbte.idde.kdim1928.backend.dao.DaoFactory;
import edu.bbte.idde.kdim1928.backend.dao.PlaceDao;
import edu.bbte.idde.kdim1928.backend.dao.TripDao;
import edu.bbte.idde.kdim1928.backend.model.Place;
import edu.bbte.idde.kdim1928.backend.model.Trip;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.sql.Date;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        DaoFactory daoFactory = DaoFactory.getInstance();

        PlaceDao placeDao = daoFactory.getPlaceDao();
        placeDao.create(new Place("Hawai", "Hawai"));

        LOGGER.info("Trips in Country Romania:");
        for (Place place1 : placeDao.findByCountry("Romania")) {
            LOGGER.info(place1.toString());
        }

        Place place = placeDao.findById(1L);

        place.setName("Budapest");
        place.setCountry("Magyarorszag");
        placeDao.update(place);

        place = placeDao.findById(2L);

        placeDao.delete(place);

        for (Place place1 : placeDao.findAll()) {
            LOGGER.info(place1.toString());
        }

        place = placeDao.findById(2L);

        TripDao tripDao = daoFactory.getTripDao();
        tripDao.create(new Trip(place,new Date(2023, 1,22),24,Boolean.FALSE,1));
        tripDao.create(new Trip(place,new Date(2023, 1,22),24,Boolean.FALSE,1));

        LOGGER.info("Trips without tour guide:");
        for (Trip trip1 : tripDao.findByRecommendTourGuide(Boolean.FALSE)) {
            LOGGER.info(trip1.toString());
        }

        place = placeDao.findById(3L);

        Trip trip = tripDao.findById(3L);
        trip.setPlace(place);
        tripDao.update(trip);

        trip = tripDao.findById(1L);
        tripDao.delete(trip);

        LOGGER.info("Trips:");
        for (Trip trip1 : tripDao.findAll()) {
            LOGGER.info(trip1.toString());
        }
    }
}
