package edu.bbte.idde.kdim1928.spring.mapper;

import edu.bbte.idde.kdim1928.spring.dto.PlaceDto;
import edu.bbte.idde.kdim1928.spring.dto.PlaceOutDto;
import edu.bbte.idde.kdim1928.spring.model.Place;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-02T13:04:47+0200",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.5.1.jar, environment: Java 17.0.5 (Oracle Corporation)"
)
@Component
public class PlaceMapperImpl extends PlaceMapper {

    @Override
    public Place placeFromDto(PlaceDto placeDto) {
        if ( placeDto == null ) {
            return null;
        }

        Place place = new Place();

        place.setName( placeDto.getName() );
        place.setCountry( placeDto.getCountry() );

        return place;
    }

    @Override
    public PlaceOutDto dtoFromPlace(Place place) {
        if ( place == null ) {
            return null;
        }

        PlaceOutDto placeOutDto = new PlaceOutDto();

        placeOutDto.setId( place.getId() );
        placeOutDto.setName( place.getName() );
        placeOutDto.setCountry( place.getCountry() );
        placeOutDto.setCreationDate( place.getCreationDate() );

        return placeOutDto;
    }

    @Override
    public Collection<PlaceOutDto> dtosFromPlaces(Collection<Place> places) {
        if ( places == null ) {
            return null;
        }

        Collection<PlaceOutDto> collection = new ArrayList<PlaceOutDto>( places.size() );
        for ( Place place : places ) {
            collection.add( dtoFromPlace( place ) );
        }

        return collection;
    }
}
