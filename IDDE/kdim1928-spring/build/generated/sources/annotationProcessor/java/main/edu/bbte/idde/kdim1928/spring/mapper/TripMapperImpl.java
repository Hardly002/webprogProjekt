package edu.bbte.idde.kdim1928.spring.mapper;

import edu.bbte.idde.kdim1928.spring.dto.PlaceDto;
import edu.bbte.idde.kdim1928.spring.dto.TripDto;
import edu.bbte.idde.kdim1928.spring.dto.TripDtoWithoutPlace;
import edu.bbte.idde.kdim1928.spring.dto.TripOutDto;
import edu.bbte.idde.kdim1928.spring.model.Place;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-02T13:04:47+0200",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.5.1.jar, environment: Java 17.0.5 (Oracle Corporation)"
)
@Component
public class TripMapperImpl extends TripMapper {

    @Override
    public Trip tripFromDto(TripDto tripDto) {
        if ( tripDto == null ) {
            return null;
        }

        Trip trip = new Trip();

        trip.setPlace( tripDto.getPlace() );
        trip.setDate( tripDto.getDate() );
        trip.setPrice( tripDto.getPrice() );
        trip.setRecommendTourGuide( tripDto.getRecommendTourGuide() );
        trip.setDifficulty( tripDto.getDifficulty() );

        return trip;
    }

    @Override
    public Trip tripFromDto(TripDtoWithoutPlace tripDtoWithoutPlace) {
        if ( tripDtoWithoutPlace == null ) {
            return null;
        }

        Trip trip = new Trip();

        trip.setDate( tripDtoWithoutPlace.getDate() );
        trip.setPrice( tripDtoWithoutPlace.getPrice() );
        trip.setRecommendTourGuide( tripDtoWithoutPlace.getRecommendTourGuide() );
        trip.setDifficulty( tripDtoWithoutPlace.getDifficulty() );

        return trip;
    }

    @Override
    public TripOutDto dtoFromTrip(Trip trip) {
        if ( trip == null ) {
            return null;
        }

        TripOutDto tripOutDto = new TripOutDto();

        tripOutDto.setId( trip.getId() );
        tripOutDto.setPlace( placeToPlaceDto( trip.getPlace() ) );
        if ( trip.getDate() != null ) {
            tripOutDto.setDate( new Date( trip.getDate().getTime() ) );
        }
        tripOutDto.setPrice( trip.getPrice() );
        tripOutDto.setRecommendTourGuide( trip.getRecommendTourGuide() );
        tripOutDto.setDifficulty( trip.getDifficulty() );

        return tripOutDto;
    }

    @Override
    public Collection<TripOutDto> dtosFromTrips(Collection<Trip> trips) {
        if ( trips == null ) {
            return null;
        }

        Collection<TripOutDto> collection = new ArrayList<TripOutDto>( trips.size() );
        for ( Trip trip : trips ) {
            collection.add( dtoFromTrip( trip ) );
        }

        return collection;
    }

    protected PlaceDto placeToPlaceDto(Place place) {
        if ( place == null ) {
            return null;
        }

        PlaceDto placeDto = new PlaceDto();

        placeDto.setName( place.getName() );
        placeDto.setCountry( place.getCountry() );

        return placeDto;
    }
}
