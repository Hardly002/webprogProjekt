package edu.bbte.idde.kdim1928.spring.dao.jpa;

import edu.bbte.idde.kdim1928.spring.dao.PlaceDao;
import edu.bbte.idde.kdim1928.spring.model.Place;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Profile("jpa")
@Repository
public interface JpaPlaceDao extends JpaRepository<Place, Long>, PlaceDao {
    @Query("UPDATE Place SET country = :#{#place.country}, name = :#{#place.name} WHERE id = :#{#place.id}")
    @Modifying
    @Transactional
    @Override
    void update(@Param("place") Place place);

    @Override
    default Long getId(Place entity) {
        return entity.getId();
    }

}
