package edu.bbte.idde.kdim1928.spring.dao;

import edu.bbte.idde.kdim1928.spring.model.BaseEntity;

import java.util.Collection;
import java.util.Optional;

public interface Dao<T extends BaseEntity> {
    T saveAndFlush(T entity);

    Optional<T> findById(Long id);

    Long getId(T entity);

    Collection<T> findAll();

    void update(T entity);

    void delete(T entity);

}