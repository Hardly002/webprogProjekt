package edu.bbte.idde.kdim1928.spring.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class PlaceDto {
    @NotNull
    @Length(max = 50)
    String name;

    @NotNull
    @Length(max = 50)
    String country;
}
