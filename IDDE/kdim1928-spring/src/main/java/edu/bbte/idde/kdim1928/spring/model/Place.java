package edu.bbte.idde.kdim1928.spring.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Component
@Entity
@Table
public class Place extends BaseEntity {
    @Column(nullable = false, unique = true)
    private String name;
    @Column(nullable = false)
    private String country;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "place", orphanRemoval = true)
    @JsonManagedReference
    private Collection<Trip> trips;

    public Place(String name, String country) {
        super();
        this.name = name;
        this.country = country;
    }
}
