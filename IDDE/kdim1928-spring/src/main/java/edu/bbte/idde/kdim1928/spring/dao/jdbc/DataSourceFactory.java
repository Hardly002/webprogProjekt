package edu.bbte.idde.kdim1928.spring.dao.jdbc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("!mem")
public class DataSourceFactory {

    @Value("${jdbc.url:jdbc:mysql://localhost:3306/trips?useSSL=false}")
    private  String jdbcUrl;
    @Value("${jdbc.user:root}")
    private  String jdbcUser;
    @Value("${jdbc.passwd:root}")
    private  String passwd;
    @Value("${jdbc.poolSize:10}")
    private  Integer connectionNumber;

    @Bean
    // @Scope("Prototype")
    public DataSource getDataSource() {
        HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setJdbcUrl(jdbcUrl);
        hikariConfig.setUsername(jdbcUser);
        hikariConfig.setPassword(passwd);

        hikariConfig.setMaximumPoolSize(connectionNumber);

        return new HikariDataSource(hikariConfig);
    }

}
