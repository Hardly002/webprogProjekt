package edu.bbte.idde.kdim1928.spring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@RequiredArgsConstructor
@ToString(callSuper = true)
@AllArgsConstructor
@Component
@EqualsAndHashCode
@Entity
@Table
public class Trip extends BaseEntity {
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @JsonBackReference
    @ToString.Exclude
    private Place place;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(nullable = false)
    private Integer price;
    @Column(nullable = false)
    private Boolean recommendTourGuide;
    @Column(nullable = false)
    private Integer difficulty; // number between 1 and 5, 1 is the easiest, 5 is the most difficult

}
