package edu.bbte.idde.kdim1928.spring.dao;

import edu.bbte.idde.kdim1928.spring.model.Trip;

import java.util.Collection;

public interface TripDao extends Dao<Trip> {
    Collection<Trip> findByRecommendTourGuide(Boolean recommendTourGuide);

    Collection<Trip> findByPlaceId(Long id);
}
