package edu.bbte.idde.kdim1928.spring.dto;

import lombok.Data;

@Data
public class PlaceOutDto {
    Long id;
    String name;
    String country;
}
