package edu.bbte.idde.kdim1928.spring.mapper;

import edu.bbte.idde.kdim1928.spring.model.Place;
import edu.bbte.idde.kdim1928.spring.dto.PlaceDto;
import edu.bbte.idde.kdim1928.spring.dto.PlaceOutDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class PlaceMapper {
    public abstract Place placeFromDto(PlaceDto placeDto);

    public abstract PlaceOutDto dtoFromPlace(Place place);

    @IterableMapping(elementTargetType = PlaceOutDto.class)
    public abstract Collection<PlaceOutDto> dtosFromPlaces(Collection<Place> places);
}
