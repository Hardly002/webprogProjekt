package edu.bbte.idde.kdim1928.spring.dao.memory;

import edu.bbte.idde.kdim1928.spring.dao.TripDao;
import edu.bbte.idde.kdim1928.spring.model.Place;
import edu.bbte.idde.kdim1928.spring.model.Trip;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Slf4j
@Profile("mem")
public class MemTripDao implements TripDao {
    private final Map<Long, Trip> trips;
    private final AtomicLong id;
    private static MemTripDao dao;

    public MemTripDao() {
        trips = new ConcurrentHashMap<>();
        id = new AtomicLong(0L);
    }

    @Override
    public Trip saveAndFlush(Trip trip) {
        Long id = this.id.getAndIncrement();
        trip.setId(id);
        this.trips.put(id, trip);
        log.info("Trip created with id: {}", id);
        return trip;
    }

    @Override
    public Collection<Trip> findAll() {
        if (trips.isEmpty()) {
            log.info("There is no Trip!");
        } else {
            log.info("Returning all Trips! ({})", this.trips.size());
        }
        return this.trips.values();
    }

    @Override
    public Optional<Trip> findById(Long id) {
        Trip trip = this.trips.get(id);
        if (trip != null) {
            log.info("Trip with {} id has found!", id);
            return Optional.of(trip);
        }
        log.info("Trip with {} id has not found!", id);
        return Optional.empty();
    }

    @Override
    public Long getId(Trip entity) {
        return null;
    }

    @Override
    public void update(Trip trip) {
        Trip oldTrip = findById(trip.getId()).get();
        boolean isUpdateSucceed = this.trips.replace(trip.getId(), oldTrip, trip);
        if (isUpdateSucceed) {
            log.info("Trip with {} id successfully updated!", trip.getId());
        } else {
            log.info("Trip update failed: {} id", trip.getId());
        }
    }

    @Override
    public void delete(Trip trip) {
        this.trips.remove(trip.getId());
        log.info("Trip with {} id successfully deleted!", trip.getId());
    }

    @Override
    public Collection<Trip> findByRecommendTourGuide(Boolean recommendTourGuide) {
        Collection<Trip> tripList = new ArrayList<>();
        for (Trip trip : this.trips.values()) {
            if (recommendTourGuide.equals(trip.getRecommendTourGuide())) {
                tripList.add(trip);
            }
        }
        return tripList;
    }

    @Override
    public Collection<Trip> findByPlaceId(Long id) {
        return new ArrayList<>();
    }

    public static synchronized MemTripDao getDao() {
        if (dao == null) {
            dao = new MemTripDao();
            dao.saveAndFlush(new Trip(new Place("Nagyvarad", "Romania"),new Date(2022,12,12),250,Boolean.TRUE,1));
            dao.saveAndFlush(new Trip(new Place("Retyezat", "Romania"), new Date(2023,5,22), 0,Boolean.FALSE, 5));
        }

        return dao;
    }
}
