package edu.bbte.idde.kdim1928.spring.mapper;

import edu.bbte.idde.kdim1928.spring.dto.TripDtoWithoutPlace;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import edu.bbte.idde.kdim1928.spring.dto.TripDto;
import edu.bbte.idde.kdim1928.spring.dto.TripOutDto;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class TripMapper {
    public abstract Trip tripFromDto(TripDto tripDto);

    public abstract Trip tripFromDto(TripDtoWithoutPlace tripDtoWithoutPlace);

    public abstract TripOutDto dtoFromTrip(Trip trip);

    @IterableMapping(elementTargetType = TripOutDto.class)
    public abstract Collection<TripOutDto> dtosFromTrips(Collection<Trip> trips);

}
