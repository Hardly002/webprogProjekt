package edu.bbte.idde.kdim1928.spring.controller;

import edu.bbte.idde.kdim1928.spring.controlleradvice.NotFoundException;
import edu.bbte.idde.kdim1928.spring.dao.PlaceDao;
import edu.bbte.idde.kdim1928.spring.dto.TripDtoWithoutPlace;
import edu.bbte.idde.kdim1928.spring.dto.TripOutDto;
import edu.bbte.idde.kdim1928.spring.mapper.TripMapper;
import edu.bbte.idde.kdim1928.spring.model.Place;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Profile("jpa")
@RestController
@RequestMapping("/api/places")
public class JpaController {
    @Autowired
    private PlaceDao placeDao;
    @Autowired
    private TripMapper tripMapper;

    @GetMapping("/{id}/trips")
    public Collection<TripOutDto> getTripsByPlace(@PathVariable("id") Long id) {
        Optional<Place> optionalPlace = placeDao.findById(id);
        if (optionalPlace.isEmpty()) {
            throw new NotFoundException();
        }
        return tripMapper.dtosFromTrips(optionalPlace.get().getTrips());
    }

    @PostMapping("/{id}/trips")
    public ResponseEntity<Trip> addTripByPlace(@PathVariable("id") Long id,
                                               @RequestBody @Valid TripDtoWithoutPlace tripDto) {
        Optional<Place> optionalPlace = placeDao.findById(id);
        if (optionalPlace.isEmpty()) {
            throw new NotFoundException();
        }
        Place place = optionalPlace.get();
        Trip trip = tripMapper.tripFromDto(tripDto);
        trip.setPlace(place);

        Collection<Trip> trips = place.getTrips();
        /* for (Trip trip1 : trips) {
            // a datum tipusok nem egyeznek meg akkor sem ha ugyan az a 2 datum mivel, ami az
            // adabazisbol jon vissza az Timestamp..... nem talaltam ra megoldast
            if (trip1.getRecommendTourGuide() == trip.getRecommendTourGuide()
                    && trip1.getDate() == trip.getDate() && Objects.equals(trip.getPrice(), trip1.getPrice())
                    && Objects.equals(trip1.getDifficulty(), trip.getDifficulty())) {
                throw new EntityExistsException();
            }
        }
        */
        trips.add(trip);
        place.setTrips(trips);
        placeDao.update(place);
        URI createUri = URI.create("/api/trips/" + trip.getId());
        return ResponseEntity.created(createUri).body(trip);
    }

    @DeleteMapping("/{placeId}/trips/{tripId}")
    public void deleteTripByPlace(@PathVariable("placeId") Long placeId, @PathVariable("tripId") Long tripId) {
        Optional<Place> optionalPlace = placeDao.findById(placeId);
        if (optionalPlace.isEmpty()) {
            throw new NotFoundException();
        }
        Place place = optionalPlace.get();
        Collection<Trip> trips = place.getTrips();
        if (!trips.removeIf(trip -> Objects.equals(trip.getId(), tripId))) {
            throw new NotFoundException();
        }
        place.setTrips(trips);
        placeDao.update(place);
    }

}
