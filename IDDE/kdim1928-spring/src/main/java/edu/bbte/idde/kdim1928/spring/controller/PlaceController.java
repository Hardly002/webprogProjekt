package edu.bbte.idde.kdim1928.spring.controller;

import edu.bbte.idde.kdim1928.spring.controlleradvice.EntityExistsException;
import edu.bbte.idde.kdim1928.spring.controlleradvice.IntegrityConstraintViolationException;
import edu.bbte.idde.kdim1928.spring.controlleradvice.NotFoundException;
import edu.bbte.idde.kdim1928.spring.dao.PlaceDao;
import edu.bbte.idde.kdim1928.spring.dao.TripDao;
import edu.bbte.idde.kdim1928.spring.model.Place;
import edu.bbte.idde.kdim1928.spring.mapper.PlaceMapper;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import edu.bbte.idde.kdim1928.spring.dto.PlaceDto;
import edu.bbte.idde.kdim1928.spring.dto.PlaceOutDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api/places")
@Slf4j
public class PlaceController {

    @Autowired
    private PlaceDao placeDao;

    @Autowired
    private TripDao tripDao;

    @Autowired
    private PlaceMapper placeMapper;

    @GetMapping
    public Collection<PlaceOutDto> findAll(@RequestParam(required = false) String country) {
        Collection<Place> places;
        if (country == null) {
            places = placeDao.findAll();
            return placeMapper.dtosFromPlaces(places);
        }
        places = placeDao.findByCountry(country);
        if (places.isEmpty()) {
            throw  new NotFoundException();
        }
        return placeMapper.dtosFromPlaces(places);
    }

    @PostMapping
    public ResponseEntity<Place> create(@RequestBody @Valid PlaceDto placeDto) {
        Place place = placeMapper.placeFromDto(placeDto);
        if (placeDao.getId(place) != null) {
            throw new EntityExistsException();
        }

        place = placeDao.saveAndFlush(placeMapper.placeFromDto(placeDto));
        URI createUri = URI.create("/api/places/" + place.getId());
        return ResponseEntity.created(createUri).body(place);
    }

    @GetMapping("/{id}")
    public PlaceOutDto findById(@PathVariable("id") Long id) {
        Optional<Place> result = placeDao.findById(id);

        if (result.isEmpty()) {
            throw new NotFoundException();
        }

        return placeMapper.dtoFromPlace(result.get());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        Optional<Place> place = placeDao.findById(id);

        if (place.isEmpty()) {
            throw new NotFoundException();
        }

        Collection<Trip> trips = tripDao.findByPlaceId(id);
        if (!trips.isEmpty()) {
            // Ha a trip tablaban hivatkozva van ra akkor nem lehet torolni
            throw new IntegrityConstraintViolationException();
        }

        placeDao.delete(place.get());
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long id,@RequestBody @Valid PlaceDto placeDto) {
        Place place = placeMapper.placeFromDto(placeDto);
        place.setId(id);
        log.info("{} - update place", place);
        placeDao.update(place);
    }
}
