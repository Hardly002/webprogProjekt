package edu.bbte.idde.kdim1928.spring.controller;

import edu.bbte.idde.kdim1928.spring.controlleradvice.NotFoundException;
import edu.bbte.idde.kdim1928.spring.dao.TripDao;
import edu.bbte.idde.kdim1928.spring.dto.TripDto;
import edu.bbte.idde.kdim1928.spring.dto.TripOutDto;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import edu.bbte.idde.kdim1928.spring.mapper.TripMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api/trips")
public class TripController {
    @Autowired
    private TripDao tripDao;

    @Autowired
    private TripMapper tripMapper;

    @GetMapping
    public Collection<TripOutDto> findall(@RequestParam(required = false)Boolean recommendTourGuide) {
        Collection<Trip> trips;
        if (recommendTourGuide == null) {
            trips = tripDao.findAll();
            return tripMapper.dtosFromTrips(trips);
        }
        trips = tripDao.findByRecommendTourGuide(recommendTourGuide);
        if (trips.isEmpty()) {
            throw new NotFoundException();
        }
        return tripMapper.dtosFromTrips(trips);
    }

    @GetMapping("/{id}")
    public TripOutDto findById(@PathVariable("id") Long id) {
        Optional<Trip> result = tripDao.findById(id);

        if (result.isEmpty()) {
            throw new NotFoundException();
        }

        return tripMapper.dtoFromTrip(result.get());
    }

    @PostMapping
    public ResponseEntity<Trip> create(@RequestBody @Valid TripDto tripDto) {
        Trip trip = tripMapper.tripFromDto(tripDto);
        trip = tripDao.saveAndFlush(trip);
        URI createUri = URI.create("/api/trips/" + trip.getId());
        return ResponseEntity.created(createUri).body(trip);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        Optional<Trip> trip = tripDao.findById(id);
        if (trip.isEmpty()) {
            throw new NotFoundException();
        }

        tripDao.delete(trip.get());
    }

    @PutMapping("/{id}")
    public void update(@RequestBody @Valid TripDto tripDto, @PathVariable Long id) {
        Trip trip = tripMapper.tripFromDto(tripDto);
        trip.setId(id);
        tripDao.update(trip);
    }
}
