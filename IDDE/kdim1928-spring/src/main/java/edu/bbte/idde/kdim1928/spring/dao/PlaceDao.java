package edu.bbte.idde.kdim1928.spring.dao;

import edu.bbte.idde.kdim1928.spring.model.Place;

import java.util.Collection;

public interface PlaceDao extends Dao<Place> {
    Collection<Place> findByCountry(String country);
}
