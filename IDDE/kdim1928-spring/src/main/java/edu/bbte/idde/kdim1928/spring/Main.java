package edu.bbte.idde.kdim1928.spring;

import edu.bbte.idde.kdim1928.spring.dao.PlaceDao;
import edu.bbte.idde.kdim1928.spring.dao.TripDao;
import edu.bbte.idde.kdim1928.spring.model.Place;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;

@SpringBootApplication
public class Main {
    @Autowired
    private PlaceDao placeDao;

    @Autowired
    private TripDao tripDao;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @PostConstruct
    public void postConstruct() {
        Place place = placeDao.saveAndFlush(new Place("Hawai", "Hawai"));
        //place.setName("Monte Carlo");
        // placeDao.update(place);

        tripDao.saveAndFlush(new Trip(place, new Date(2023, Calendar.MARCH, 22), 2000, Boolean.TRUE, 5));
        // trip.setPrice(4000);
        // tripDao.update(trip);
    }

}