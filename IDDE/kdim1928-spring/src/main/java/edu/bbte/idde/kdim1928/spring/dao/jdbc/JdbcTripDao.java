package edu.bbte.idde.kdim1928.spring.dao.jdbc;

import edu.bbte.idde.kdim1928.spring.controlleradvice.EntityExistsException;
import edu.bbte.idde.kdim1928.spring.controlleradvice.JdbcConnectionException;
import edu.bbte.idde.kdim1928.spring.controlleradvice.NotFoundException;
import edu.bbte.idde.kdim1928.spring.dao.TripDao;
import edu.bbte.idde.kdim1928.spring.model.Place;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Repository
@Profile("jdbc")
@Slf4j
public class JdbcTripDao implements TripDao {
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcPlaceDao jdbcPlaceDao;

    @Override
    public Long getId(Trip trip) {
        if (jdbcPlaceDao.getId(trip.getPlace()) == null) {
            return null;
        }
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select id from Trip where placeid = ? and date = ? and "
                            + "price = ? and recommendTourGuide = ? and difficulty = ?");
            prep.setLong(1, jdbcPlaceDao.getId(trip.getPlace()));
            prep.setDate(2, (Date) trip.getDate());
            prep.setInt(3, trip.getPrice());
            prep.setBoolean(4, trip.getRecommendTourGuide());
            prep.setInt(5, trip.getDifficulty());
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                return set.getLong("id");
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        return null;
    }

    @Override
    public Trip saveAndFlush(Trip trip) {
        /*Optional<Place> placeOptional = jdbcPlaceDao.findById(trip.getPlace().getId());
        Place place;
        if (placeOptional.isEmpty()) {
            place = jdbcPlaceDao.saveAndFlush(trip.getPlace());
        } else {
            place = placeOptional.get();
        }
        trip.setPlace(place);
        */

        Long placeId = jdbcPlaceDao.getId(trip.getPlace());
        if (placeId == null) {
            placeId = jdbcPlaceDao.saveAndFlush(trip.getPlace()).getId();
        }
        Place place = trip.getPlace();
        place.setId(placeId);
        trip.setPlace(place);

        if (getId(trip) != null) {
            trip.setId(getId(trip));
            log.info("Hiba a Trip letezik mar az adabazisban! ::: {}", trip.toString());
            throw new EntityExistsException();
        }

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("insert into Trip (placeid, date, price, recommendTourGuide, difficulty)"
                            + " values(?, ?, ?, ?, ?)");
            prep.setLong(1, place.getId());
            prep.setDate(2, (Date) trip.getDate());
            prep.setInt(3, trip.getPrice());
            prep.setBoolean(4, trip.getRecommendTourGuide());
            prep.setInt(5, trip.getDifficulty());
            prep.executeUpdate();
            log.info("A {} - idval rendelkezo TRIP sikeresen hozzaadva!", getId(trip));
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        log.info("{} ez a TRIP", trip);
        trip.setId(getId(trip));
        return trip;
    }

    @Override
    public Optional<Trip> findById(Long id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Trip where id = ?");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                Trip trip = getTripFromResultSet(set);
                log.info("Trip with {} id has found ::: {}", id, trip);
                return Optional.of(trip);
            }

        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }

        return Optional.empty();
    }

    @Override
    public Collection<Trip> findByRecommendTourGuide(Boolean recommendTourGuide) {
        Collection<Trip> trips = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Trip WHERE recommendTourGuide = ?");
            prep.setBoolean(1,recommendTourGuide);
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                trips.add(getTripFromResultSet(set));
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        return trips;
    }

    @Override
    public Collection<Trip> findByPlaceId(Long id) {
        Collection<Trip> trips = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Trip WHERE placeid = ?");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                trips.add(getTripFromResultSet(set));
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        return trips;
    }

    @Override
    public Collection<Trip> findAll() {
        Collection<Trip> trips = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Trip");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                trips.add(getTripFromResultSet(set));
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        return trips;
    }

    @Override
    public void update(Trip trip) {
        if (findById(trip.getId()).isEmpty()) {
            log.info("Hiba a Trip NEM letezik ::: id = {}", trip.getId());
        }

        createPlaceIfNotExists(trip.getPlace());

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("UPDATE Trip SET placeid = ?, date = ?, "
                            + "price = ?, recommendTourGuide = ?, difficulty = ? WHERE id = ?");
            prep.setLong(1, jdbcPlaceDao.getId(trip.getPlace()));
            prep.setDate(2, (Date) trip.getDate());
            prep.setInt(3, trip.getPrice());
            prep.setBoolean(4, trip.getRecommendTourGuide());
            prep.setInt(5, trip.getDifficulty());
            prep.setLong(6, trip.getId());
            // Ha nem modositott egy sort sem akkor nem letezik ${id} - val trip
            // => igy szolok a controllernek, hogy baj van 404
            if (prep.executeUpdate() == 0) {
                throw new NotFoundException();
            }
            log.info("Trip with id = {} successfully updated", trip.getId());
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
    }

    @Override
    public void delete(Trip trip) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("DELETE FROM Trip WHERE id = ?");
            prep.setLong(1, trip.getId());
            prep.executeUpdate();
            log.info("Trip with id = {} successfully DELETED", trip.getId());
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
    }

    private Trip getTripFromResultSet(ResultSet set) throws SQLException {
        Place place = jdbcPlaceDao.findById(set.getLong("placeid")).get();
        Trip trip = new Trip(place,
                set.getDate("date"),
                set.getInt("price"),
                set.getBoolean("recommendTourGuide"),
                set.getInt("difficulty"));
        trip.setId(set.getLong("id"));
        return trip;
    }

    private void createPlaceIfNotExists(Place place) {
        if (jdbcPlaceDao.getId(place) == null) {
            // ha nem letezik a Place akkor letrehozom
            jdbcPlaceDao.saveAndFlush(place);
        }
    }
}
