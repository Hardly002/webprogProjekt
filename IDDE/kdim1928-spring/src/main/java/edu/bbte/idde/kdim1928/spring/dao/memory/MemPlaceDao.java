package edu.bbte.idde.kdim1928.spring.dao.memory;

import edu.bbte.idde.kdim1928.spring.dao.PlaceDao;
import edu.bbte.idde.kdim1928.spring.model.Place;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Slf4j
@Profile("mem")
public class MemPlaceDao implements PlaceDao {
    private final Map<Long, Place> places;
    private final AtomicLong id;

    public MemPlaceDao() {
        places = new ConcurrentHashMap<>();
        id = new AtomicLong(0L);
    }

    @Override
    public Place saveAndFlush(Place place) {
        Long id = this.id.getAndIncrement();
        place.setId(id);
        this.places.put(id, place);
        log.info("Place created with id: {}", id);
        return place;
    }

    @Override
    public Optional<Place> findById(Long id) {
        Place place = this.places.get(id);
        if (place != null) {
            log.info("Place with {} id has found!", id);
            return Optional.of(place);
        }
        log.info("Place with {} id has not found!", id);
        return Optional.empty();
    }

    @Override
    public Long getId(Place entity) {
        return null;
    }

    @Override
    public Collection<Place> findAll() {
        if (places.isEmpty()) {
            log.info("There is no Places!");
        } else {
            log.info("Returning all Places! ({})", this.places.size());
        }
        return this.places.values();
    }

    @Override
    public void update(Place place) {
        Optional<Place> oldPlace = findById(place.getId());
        if (oldPlace.isPresent() && this.places.replace(place.getId(), oldPlace.get(), place)) {
            log.info("Place with {} id successfully updated!", place.getId());
        } else {
            log.info("Place update failed: {} id", place.getId());
        }
    }

    @Override
    public void delete(Place place) {
        this.places.remove(place.getId());
        log.info("Place with {} id successfully deleted!", place.getId());
    }

    @Override
    public Collection<Place> findByCountry(String country) {
        Collection<Place> placeList = new ArrayList<>();
        for (Place place : this.places.values()) {
            if (country.equals(place.getCountry())) {
                placeList.add(place);
            }
        }
        if (placeList.isEmpty()) {
            log.info("There is no Places in Country: {}!", country);
        }
        return placeList;
    }
}
