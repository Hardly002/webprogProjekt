package edu.bbte.idde.kdim1928.spring.dao.jdbc;

import edu.bbte.idde.kdim1928.spring.controlleradvice.JdbcConnectionException;
import edu.bbte.idde.kdim1928.spring.controlleradvice.NotFoundException;
import edu.bbte.idde.kdim1928.spring.dao.PlaceDao;
import edu.bbte.idde.kdim1928.spring.model.Place;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Repository
@Profile("jdbc")
@Slf4j
public class JdbcPlaceDao implements PlaceDao {
    @Autowired
    private DataSource dataSource;

    @Override
    public Long getId(Place place) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select id from Place where name = ? and country = ?");
            prep.setString(1, place.getName());
            prep.setString(2, place.getCountry());
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                return set.getLong("id");
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
        }
        return null;
    }

    @Override
    public Place saveAndFlush(Place place) {
        if (getId(place) != null) {
            log.info("Hiba: A Place mar letezik az adabazisban! ::: {}", place.toString());
            return place;
        }
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("insert into Place (name, country) values(?, ?)");
            prep.setString(1, place.getName());
            prep.setString(2, place.getCountry());
            prep.executeUpdate();
            place.setId(getId(place));
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
        }
        return place;
    }

    @Override
    public Optional<Place> findById(Long id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select id, name, country from Place where id = ?");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                Place place = getPlaceFromResultSet(set);
                place.setId(id);
                log.info("Place with id {} has found: {}", id, place);
                return Optional.of(place);
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        return Optional.empty();
    }

    @Override
    public Collection<Place> findAll() {
        Collection<Place> places = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Place");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                places.add(getPlaceFromResultSet(set));
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        return places;

    }

    @Override
    public void update(Place place) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("UPDATE Place SET name = ?, country = ? WHERE id = ?");
            prep.setString(1, place.getName());
            prep.setString(2, place.getCountry());
            prep.setLong(3, place.getId());
            // Ha nem modositott egy sort sem akkor nem letezik ${id} - val place
            // => igy szolok a controllernek, hogy HELLO
            if (prep.executeUpdate() == 0) {
                throw new NotFoundException();
            }
            log.info("Place with id = {} successfully updated", place.getId());
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
    }

    @Override
    public void delete(Place place) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("DELETE FROM Place WHERE id = ?");
            prep.setLong(1, place.getId());
            prep.executeUpdate();
            log.info("Place with id = {} successfully DELETED", place.getId());
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
    }

    @Override
    public Collection<Place> findByCountry(String country) {
        Collection<Place> places = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Place WHERE country = ?");
            prep.setString(1, country);
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                places.add(getPlaceFromResultSet(set));
            }
        } catch (SQLException e) {
            log.error("Hiba: {}", e.toString());
            throw new JdbcConnectionException();
        }
        return places;
    }

    private Place getPlaceFromResultSet(ResultSet set) throws SQLException {
        Place place = new Place(set.getString("name"),
                set.getString("country"));
        place.setId(set.getLong("id"));
        return place;
    }

    /*private PreparedStatement getPreparedStatement(String sql, Place place) throws java.sql.SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement prep = connection.prepareStatement(sql);
        prep.set
    }*/
}
