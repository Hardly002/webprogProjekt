package edu.bbte.idde.kdim1928.spring.dto;

import edu.bbte.idde.kdim1928.spring.model.Place;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.sql.Date;

@Data
@ToString(callSuper = true)
public class TripDto {
    @NotNull
    Place place;

    @NotNull
    Date date;

    @NotNull
    @Positive
    @Max(10000)
    Integer price;

    @NotNull
    Boolean recommendTourGuide;

    @NotNull
    @Positive
    Integer difficulty;
}
