package edu.bbte.idde.kdim1928.spring.dto;

import lombok.Data;
import lombok.ToString;

import java.sql.Date;

@Data
@ToString(callSuper = true)
public class TripOutDto {
    Long id;
    PlaceDto place;
    Date date;
    Integer price;
    Boolean recommendTourGuide;
    Integer difficulty;
}
