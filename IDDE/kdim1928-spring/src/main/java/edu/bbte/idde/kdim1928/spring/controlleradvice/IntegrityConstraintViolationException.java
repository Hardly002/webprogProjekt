package edu.bbte.idde.kdim1928.spring.controlleradvice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class IntegrityConstraintViolationException extends RuntimeException {
}
