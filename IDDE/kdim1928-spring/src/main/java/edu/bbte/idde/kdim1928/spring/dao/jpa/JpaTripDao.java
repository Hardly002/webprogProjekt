package edu.bbte.idde.kdim1928.spring.dao.jpa;

import edu.bbte.idde.kdim1928.spring.dao.TripDao;
import edu.bbte.idde.kdim1928.spring.model.Trip;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Profile("jpa")
@Repository
public interface JpaTripDao extends JpaRepository<Trip, Long>, TripDao {
    @Override
    default Long getId(Trip entity) {
        return entity.getId();
    }

    @Query("UPDATE Trip SET place = :#{#trip.place}, date = :#{#trip.date}, price = :#{#trip.price}, "
            + "recommendTourGuide = :#{#trip.recommendTourGuide}, "
            + "difficulty = :#{#trip.difficulty} WHERE id = :#{#trip.id}")
    @Modifying
    @Transactional
    @Override
    void update(@Param("trip") Trip entity);

}
