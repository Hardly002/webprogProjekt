package edu.bbte.idde.kdim1928.backend.model;

import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Place extends BaseEntity {
    private String name;
    private String country;
}
