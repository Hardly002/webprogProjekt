package edu.bbte.idde.kdim1928.backend.dao.memory;

import edu.bbte.idde.kdim1928.backend.dao.DaoFactory;
import edu.bbte.idde.kdim1928.backend.model.Place;
import edu.bbte.idde.kdim1928.backend.model.Trip;

import java.util.Date;

public class MemDaoFactory extends DaoFactory {
    private static MemTripDao tripDao;
    private static MemPlaceDao placeDao;

    @Override
    public synchronized MemTripDao getTripDao() {
        if (tripDao == null) {
            // nem vagyok biztos benne hogy ezt igy kell csinalni:))
            if (placeDao == null) {
                placeDao = new MemPlaceDao();
                placeDao.create(new Place("Nagyvarad", "Romania"));
                placeDao.create(new Place("Retyezat", "Romania"));
            }
            tripDao = new MemTripDao();
            tripDao.create(new Trip(placeDao.findById(0L),new Date(2022,12,12),250,Boolean.TRUE,1));
            tripDao.create(new Trip(placeDao.findById(1L), new Date(2023,5,22), 0,Boolean.FALSE, 5));
        }
        return tripDao;
    }

    @Override
    public synchronized MemPlaceDao getPlaceDao() {
        if (placeDao == null) {
            placeDao = new MemPlaceDao();
            placeDao.create(new Place("Nagyvarad", "Romania"));
            placeDao.create(new Place("Retyezat", "Romania"));
        }
        return placeDao;
    }
}
