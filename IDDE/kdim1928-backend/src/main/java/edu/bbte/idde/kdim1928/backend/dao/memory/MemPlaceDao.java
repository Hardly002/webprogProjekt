package edu.bbte.idde.kdim1928.backend.dao.memory;

import edu.bbte.idde.kdim1928.backend.dao.PlaceDao;
import edu.bbte.idde.kdim1928.backend.model.Place;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MemPlaceDao implements PlaceDao {
    private final Map<Long, Place> places;
    private final AtomicLong id;
    private final Logger log;

    public MemPlaceDao() {
        places = new ConcurrentHashMap<>();
        id = new AtomicLong(0L);
        log = LoggerFactory.getLogger(MemTripDao.class);
    }

    @Override
    public void create(Place place) {
        Long id = this.id.getAndIncrement();
        place.setId(id);
        this.places.put(id, place);
        log.info("Place created with id: {}", id);
    }

    @Override
    public Place findById(Long id) {
        Place place = this.places.get(id);
        if (place != null) {
            log.info("Place with {} id has found!", id);
            return place;
        }
        log.info("Place with {} id has not found!", id);
        return null;
    }

    @Override
    public Long getId(Place entity) {
        return null;
    }

    @Override
    public Collection<Place> findAll() {
        if (places.isEmpty()) {
            log.info("There is no Places!");
        } else {
            log.info("Returning all Places! ({})", this.places.size());
        }
        return this.places.values();
    }

    @Override
    public Boolean update(Place place) {
        Place oldPlace = findById(place.getId());
        Boolean isUpdateSucceeded = this.places.replace(place.getId(), oldPlace, place);
        if (isUpdateSucceeded) {
            log.info("Place with {} id successfully updated!", place.getId());
        } else {
            log.info("Place update failed: {} id", place.getId());
        }
        return isUpdateSucceeded;
    }

    @Override
    public void delete(Place place) {
        this.places.remove(place.getId());
        log.info("Place with {} id successfully deleted!", place.getId());
    }

    @Override
    public Collection<Place> findByCountry(String country) {
        Collection<Place> placeList = new ArrayList<>();
        for (Place place : this.places.values()) {
            if (country.equals(place.getCountry())) {
                placeList.add(place);
            }
        }
        if (placeList.isEmpty()) {
            log.info("There is no Places in Country: {}!", country);
        }
        return placeList;
    }
}
