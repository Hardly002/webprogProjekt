package edu.bbte.idde.kdim1928.backend.dao.jdbc;

import edu.bbte.idde.kdim1928.backend.dao.PlaceDao;
import edu.bbte.idde.kdim1928.backend.model.Place;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class JdbcPlaceDao implements PlaceDao {
    private final DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(JdbcTripDao.class);

    public JdbcPlaceDao() {
        dataSource = DataSourceFactory.getDataSource();
    }

    @Override
    public Long getId(Place place) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select id from Places where name = ? and country = ?");
            prep.setString(1, place.getName());
            prep.setString(2, place.getCountry());
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                return set.getLong("id");
            }
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return null;
    }

    @Override
    public void create(Place place) {
        if (getId(place) != null) {
            LOG.info("Hiba: A Place mar letezik az adabazisban! ::: {}", place.toString());
        }
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("insert into Places (name, country) values(?, ?)");
            prep.setString(1, place.getName());
            prep.setString(2, place.getCountry());
            prep.executeUpdate();
            place.setId(getId(place));
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public Place findById(Long id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select id, name, country from Places where id = ?");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                Place place = getPlaceFromResultSet(set);
                place.setId(id);
                LOG.info("Place with id {} has found: {}", id, place);
                return place;
            }
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return null;
    }

    @Override
    public Collection<Place> findAll() {
        Collection<Place> places = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Places");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                places.add(getPlaceFromResultSet(set));
            }
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return places;

    }

    @Override
    public Boolean update(Place place) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("UPDATE Places SET name = ?, country = ? WHERE id = ?");
            prep.setString(1, place.getName());
            prep.setString(2, place.getCountry());
            prep.setLong(3, place.getId());
            prep.executeUpdate();
            LOG.info("Place with id = {} successfully updated", place.getId());
            return Boolean.TRUE;
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return Boolean.FALSE;
    }

    @Override
    public void delete(Place place) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("DELETE FROM Places WHERE id = ?");
            prep.setLong(1, place.getId());
            prep.executeUpdate();
            LOG.info("Place with id = {} successfully DELETED", place.getId());
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public Collection<Place> findByCountry(String country) {
        Collection<Place> places = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Places WHERE country = ?");
            prep.setString(1, country);
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                places.add(getPlaceFromResultSet(set));
            }
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return places;
    }

    private Place getPlaceFromResultSet(ResultSet set) throws SQLException {
        Place place = new Place(set.getString("name"),
                set.getString("country"));
        place.setId(set.getLong("id"));
        return place;
    }

    /*private PreparedStatement getPreparedStatement(String sql, Place place) throws java.sql.SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement prep = connection.prepareStatement(sql);
        prep.set
    }*/
}
