package edu.bbte.idde.kdim1928.backend.dao.jdbc;

import edu.bbte.idde.kdim1928.backend.dao.TripDao;
import edu.bbte.idde.kdim1928.backend.model.Place;
import edu.bbte.idde.kdim1928.backend.model.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class JdbcTripDao implements TripDao {
    private final DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(JdbcTripDao.class);

    public JdbcTripDao() {
        dataSource = DataSourceFactory.getDataSource();
    }

    @Override
    public Long getId(Trip trip) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select id from Trips where placeid = ? and date = ? and "
                            + "price = ? and recommendTourGuide = ? and difficulty = ?");
            prep.setLong(1, JdbcDaoFactory.getInstance().getPlaceDao().getId(trip.getPlace()));
            prep.setDate(2, (Date) trip.getDate());
            prep.setInt(3, trip.getPrice());
            prep.setBoolean(4, trip.getRecommendTourGuide());
            prep.setInt(5, trip.getDifficulty());
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                return set.getLong("id");
            }
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return null;
    }

    @Override
    public void create(Trip trip) {
        if (JdbcDaoFactory.getInstance().getPlaceDao().getId(trip.getPlace()) != null && getId(trip) != null) {
            trip.setId(getId(trip));
            LOG.info("Hiba a Trip letezik mar az adabazisban! ::: {}", trip.toString());
            return;
        }

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("insert into Trips (placeid, date, price, recommendTourGuide, difficulty)"
                            + " values(?, ?, ?, ?, ?)");
            if (JdbcDaoFactory.getInstance().getPlaceDao().getId(trip.getPlace()) == null) {
                // ha nem letezik a Place akkor letrehozom
                JdbcDaoFactory.getInstance().getPlaceDao().create(trip.getPlace());
            }
            prep.setLong(1, trip.getPlace().getId());
            prep.setDate(2, (Date) trip.getDate());
            prep.setInt(3, trip.getPrice());
            prep.setBoolean(4, trip.getRecommendTourGuide());
            prep.setInt(5, trip.getDifficulty());
            prep.executeUpdate();
            LOG.info("A {} - idval rendelkezo TRIP sikeresen hozzaadva!", getId(trip));
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    @Override
    public Trip findById(Long id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Trips where id = ?");
            prep.setLong(1, id);
            ResultSet set = prep.executeQuery();
            if (set.next()) {
                Trip trip = getTripFromResultSet(set);
                LOG.info("Trip with {} id has found ::: {}", id, trip);
                return trip;
            }

        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }

        return null;
    }

    @Override
    public Collection<Trip> findByRecommendTourGuide(Boolean recommendTourGuide) {
        Collection<Trip> trips = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Trips WHERE recommendTourGuide = ?");
            prep.setBoolean(1,recommendTourGuide);
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                trips.add(getTripFromResultSet(set));
            }
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return trips;
    }

    @Override
    public Collection<Trip> findAll() {
        Collection<Trip> trips = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("select * from Trips");
            ResultSet set = prep.executeQuery();
            while (set.next()) {
                trips.add(getTripFromResultSet(set));
            }
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return trips;
    }

    @Override
    public Boolean update(Trip trip) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("UPDATE Trips SET placeid = ?, date = ?, "
                            + "price = ?, recommendTourGuide = ?, difficulty = ? WHERE id = ?");
            prep.setLong(1, trip.getPlace().getId());
            prep.setDate(2, (Date) trip.getDate());
            prep.setInt(3, trip.getPrice());
            prep.setBoolean(4, trip.getRecommendTourGuide());
            prep.setInt(5, trip.getDifficulty());
            prep.setLong(6, trip.getId());
            prep.executeUpdate();
            LOG.info("Trip with id = {} successfully updated", trip.getId());
            return Boolean.TRUE;
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
        return Boolean.FALSE;
    }

    @Override
    public void delete(Trip trip) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement prep = connection
                    .prepareStatement("DELETE FROM Trips WHERE id = ?");
            prep.setLong(1, trip.getId());
            prep.executeUpdate();
            LOG.info("Trip with id = {} successfully DELETED", trip.getId());
        } catch (SQLException e) {
            LOG.error("Hiba: {}", e.toString());
        }
    }

    private Trip getTripFromResultSet(ResultSet set) throws SQLException {
        Place place = JdbcDaoFactory.getInstance().getPlaceDao().findById(set.getLong("placeid"));
        Trip trip = new Trip(place,
                set.getDate("date"),
                set.getInt("price"),
                set.getBoolean("recommendTourGuide"),
                set.getInt("difficulty"));
        trip.setId(set.getLong("id"));
        return trip;
    }
}
