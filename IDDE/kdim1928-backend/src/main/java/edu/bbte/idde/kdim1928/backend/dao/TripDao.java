package edu.bbte.idde.kdim1928.backend.dao;

import edu.bbte.idde.kdim1928.backend.model.Trip;

import java.util.Collection;

public interface TripDao extends Dao<Trip> {
    Collection<Trip> findByRecommendTourGuide(Boolean recommendTourGuide);
}
