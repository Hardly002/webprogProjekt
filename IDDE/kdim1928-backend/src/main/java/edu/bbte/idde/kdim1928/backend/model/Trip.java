package edu.bbte.idde.kdim1928.backend.model;

import lombok.*;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Trip extends BaseEntity {
    private Place place;
    private Date date;
    private Integer price;
    private Boolean recommendTourGuide;
    private Integer difficulty; // number between 1 and 5, 1 is the easiest, 5 is the most difficult
}
