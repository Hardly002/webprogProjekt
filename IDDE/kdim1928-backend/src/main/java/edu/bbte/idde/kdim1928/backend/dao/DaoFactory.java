package edu.bbte.idde.kdim1928.backend.dao;

import edu.bbte.idde.kdim1928.backend.config.Config;
import edu.bbte.idde.kdim1928.backend.config.ConfigFactory;
import edu.bbte.idde.kdim1928.backend.dao.jdbc.JdbcDaoFactory;
import edu.bbte.idde.kdim1928.backend.dao.memory.MemDaoFactory;

public abstract class DaoFactory {
    private static DaoFactory instance;

    public abstract TripDao getTripDao();

    public abstract PlaceDao getPlaceDao();

    public static synchronized DaoFactory getInstance() {
        if (instance == null) {
            Config config = ConfigFactory.getConfig();

            if ("jdbc".equals(config.getDaoType())) {
                instance = new JdbcDaoFactory();
            } else {
                instance = new MemDaoFactory();
            }
        }
        return instance;
    }
}
