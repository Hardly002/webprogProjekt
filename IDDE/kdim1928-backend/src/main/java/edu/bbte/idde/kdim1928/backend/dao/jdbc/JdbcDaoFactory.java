package edu.bbte.idde.kdim1928.backend.dao.jdbc;

import edu.bbte.idde.kdim1928.backend.dao.DaoFactory;
import edu.bbte.idde.kdim1928.backend.dao.PlaceDao;
import edu.bbte.idde.kdim1928.backend.dao.TripDao;

public class JdbcDaoFactory extends DaoFactory {
    private static JdbcTripDao tripDao;
    private static JdbcPlaceDao placeDao;

    @Override
    public synchronized TripDao getTripDao() {
        if (tripDao == null) {
            tripDao = new JdbcTripDao();
        }
        return tripDao;
    }

    @Override
    public synchronized PlaceDao getPlaceDao() {
        if (placeDao == null) {
            placeDao = new JdbcPlaceDao();
        }
        return placeDao;
    }
}
