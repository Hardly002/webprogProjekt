package edu.bbte.idde.kdim1928.backend.dao;

import edu.bbte.idde.kdim1928.backend.model.Place;

import java.util.Collection;

public interface PlaceDao extends Dao<Place> {
    Collection<Place> findByCountry(String country);
}
