package edu.bbte.idde.kdim1928.backend.dao;

import edu.bbte.idde.kdim1928.backend.model.BaseEntity;

import java.util.Collection;

public interface Dao<T extends BaseEntity> {
    void create(T entity);

    T findById(Long id);

    Long getId(T entity);

    Collection<T> findAll();

    Boolean update(T entity);

    void delete(T entity);

}