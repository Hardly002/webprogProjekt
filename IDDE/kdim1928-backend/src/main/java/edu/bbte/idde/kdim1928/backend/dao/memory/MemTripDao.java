package edu.bbte.idde.kdim1928.backend.dao.memory;

import edu.bbte.idde.kdim1928.backend.dao.TripDao;
import edu.bbte.idde.kdim1928.backend.model.Place;
import edu.bbte.idde.kdim1928.backend.model.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MemTripDao implements TripDao {
    private final Map<Long, Trip> trips;
    private final AtomicLong id;
    private final Logger log;
    private static MemTripDao dao;

    public MemTripDao() {
        trips = new ConcurrentHashMap<>();
        id = new AtomicLong(0L);
        log = LoggerFactory.getLogger(MemTripDao.class);
    }

    @Override
    public void create(Trip trip) {
        Long id = this.id.getAndIncrement();
        trip.setId(id);
        this.trips.put(id, trip);
        log.info("Trip created with id: {}", id);
    }

    @Override
    public Collection<Trip> findAll() {
        if (trips.isEmpty()) {
            log.info("There is no Trip!");
        } else {
            log.info("Returning all Trips! ({})", this.trips.size());
        }
        return this.trips.values();
    }

    @Override
    public Trip findById(Long id) {
        Trip trip = this.trips.get(id);
        if (trip != null) {
            log.info("Trip with {} id has found!", id);
            return trip;
        }
        log.info("Trip with {} id has not found!", id);
        return null;
    }

    @Override
    public Long getId(Trip entity) {
        return null;
    }

    @Override
    public Boolean update(Trip trip) {
        Trip oldTrip = findById(trip.getId());
        Boolean isUpdateSucceded = this.trips.replace(trip.getId(), oldTrip, trip);
        if (isUpdateSucceded) {
            log.info("Trip with {} id successfully updated!", trip.getId());
        } else {
            log.info("Trip update failed: {} id", trip.getId());
        }
        return isUpdateSucceded;
    }

    @Override
    public void delete(Trip trip) {
        this.trips.remove(trip.getId());
        log.info("Trip with {} id successfully deleted!", trip.getId());
    }

    @Override
    public Collection<Trip> findByRecommendTourGuide(Boolean recommendTourGuide) {
        Collection<Trip> tripList = new ArrayList<>();
        for (Trip trip : this.trips.values()) {
            if (recommendTourGuide.equals(trip.getRecommendTourGuide())) {
                tripList.add(trip);
            }
        }
        return tripList;
    }

    public static synchronized MemTripDao getDao() {
        if (dao == null) {
            dao = new MemTripDao();
            dao.create(new Trip(new Place("Nagyvarad", "Romania"),new Date(2022,12,12),250,Boolean.TRUE,1));
            dao.create(new Trip(new Place("Retyezat", "Romania"), new Date(2023,5,22), 0,Boolean.FALSE, 5));
        }

        return dao;
    }
}
