package edu.bbte.idde.kdim1928.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.idde.kdim1928.backend.dao.DaoFactory;
import edu.bbte.idde.kdim1928.backend.dao.PlaceDao;
import edu.bbte.idde.kdim1928.backend.model.Place;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.NoSuchElementException;

@WebServlet("/places/json")
public class PlaceServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TripServlet.class);
    private PlaceDao placeDao;
    private ObjectMapper objectMapper;

    @Override
    public void init() throws ServletException {
        super.init();
        placeDao = DaoFactory.getInstance().getPlaceDao();
        objectMapper = ObjectMapperFactory.getObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id") == null) {
            Collection<Place> places = placeDao.findAll();
            resp.setHeader("Content-type", "application/json");
            objectMapper.writeValue(resp.getWriter(), places);
            LOG.info("{}", places);
            return;
        }

        try {
            Integer id = Integer.parseInt(req.getParameter("id"));
            Place place = placeDao.findById(id.longValue());
            if (place == null) {
                resp.sendError(404);
                return;
            }
            objectMapper.writeValue(resp.getWriter(), place);
        } catch (NumberFormatException e) {
            resp.sendError(400);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            checkContentType(req);
            Place place = objectMapper.readValue(req.getInputStream(), Place.class);
            LOG.info("Received trip: {}", place);
            if (place == null) {
                throw new IOException("Jason has not every attribute for a new Place!");
            }
            placeDao.create(place);
        } catch (IOException e) {
            LOG.info("{}", e.toString());
            resp.sendError(400);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Place place = placeDao.findById(Long.parseLong(req.getParameter("id")));
            if (place == null) {
                throw new NoSuchElementException("Place was not Found!");
            }
            placeDao.delete(place);
        } catch (NumberFormatException e) {
            LOG.info("{}", e.toString());
            resp.sendError(400);
        } catch (NoSuchElementException e) {
            LOG.info("{}", e.toString());
            resp.sendError(404);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            checkContentType(req);
            Place place = objectMapper.readValue(req.getInputStream(), Place.class);
            LOG.info("Received trip: {}", place);
            if (place == null) {
                throw new IOException("Jason has not every attribute for a new Place!");
            }
            if (!placeDao.update(place)) {
                throw new NoSuchElementException("Trip not found!");
            }
        } catch (IOException e) {
            LOG.info("{}", e.toString());
            resp.sendError(400);
        } catch (NoSuchElementException e) {
            LOG.info("{}", e.toString());
            resp.sendError(404);
        }
    }

    private void checkContentType(HttpServletRequest req) throws IOException {
        if (!"application/json".equals(req.getHeader("Content-type"))) {
            throw new IOException("Content-type is NOT application/json!");
        }
    }
}
