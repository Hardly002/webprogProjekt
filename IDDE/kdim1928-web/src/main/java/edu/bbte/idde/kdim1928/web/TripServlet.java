package edu.bbte.idde.kdim1928.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.idde.kdim1928.backend.dao.DaoFactory;
import edu.bbte.idde.kdim1928.backend.dao.TripDao;
import edu.bbte.idde.kdim1928.backend.model.Trip;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.NoSuchElementException;

@WebServlet("/trips/json")
public class TripServlet extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TripServlet.class);
    private TripDao tripDao;
    private ObjectMapper objectMapper;

    @Override
    public void init() throws ServletException {
        super.init();
        tripDao = DaoFactory.getInstance().getTripDao();
        objectMapper = ObjectMapperFactory.getObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (req.getParameter("id") == null) {
            Collection<Trip> trips = tripDao.findAll();
            resp.setHeader("Content-type", "application/json");
            objectMapper.writeValue(resp.getWriter(), trips);
            LOG.info("{}", trips);
            return;
        }

        try {
            Integer id = Integer.parseInt(req.getParameter("id"));
            Trip trip = tripDao.findById(id.longValue());
            if (trip == null) {
                resp.sendError(404);
                return;
            }
            objectMapper.writeValue(resp.getWriter(), trip);
        } catch (NumberFormatException e) {
            resp.sendError(400);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            checkContentType(req);
            Trip trip = objectMapper.readValue(req.getInputStream(), Trip.class);
            LOG.info("Received trip: {}", trip);
            if (trip == null) {
                throw new IOException("Jason has not every attribute for a new Trip!");
            }
            tripDao.create(trip);
        } catch (IOException e) {
            LOG.info("{}", e.toString());
            resp.sendError(400);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            Trip trip = tripDao.findById(Long.parseLong(req.getParameter("id")));
            if (trip == null) {
                throw new NoSuchElementException("Trip was not Found!");
            }
            tripDao.delete(trip);
        } catch (NumberFormatException e) {
            LOG.info("{}", e.toString());
            resp.sendError(400);
        } catch (NoSuchElementException e) {
            LOG.info("{}", e.toString());
            resp.sendError(404);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            checkContentType(req);
            Trip trip = objectMapper.readValue(req.getInputStream(), Trip.class);
            LOG.info("Received trip: {}", trip);
            if (trip == null) {
                throw new IOException("Jason has not every attribute for a new Trip!");
            }
            if (!tripDao.update(trip)) {
                throw new NoSuchElementException("Trip not found!");
            }
        } catch (IOException e) {
            LOG.info("{}", e.toString());
            resp.sendError(400);
        } catch (NoSuchElementException e) {
            LOG.info("{}", e.toString());
            resp.sendError(404);
        }
    }

    private void checkContentType(HttpServletRequest req) throws IOException {
        if (!"application/json".equals(req.getHeader("Content-type"))) {
            throw new IOException("Content-type is NOT application/json!");
        }
    }
}
