package edu.bbte.idde.kdim1928.web;

import com.github.jknack.handlebars.Template;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/login")
public class Login extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TripServlet.class);
    private Map<String, Object> model;
    private String username;
    private String password;

    @Override
    public void init() throws ServletException {
        username = "Attila";
        password = "12345678";
        model = new ConcurrentHashMap<>();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        model.put("fail", "");

        Template template = HandlebarsTemplateFactory.getTemplate("login");
        template.apply(model, resp.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uname = req.getParameter("uname");
        String pwd = req.getParameter("pwd");
        if (password.equals(pwd) && username.equals(uname)) {
            HttpSession session = req.getSession();
            session.setAttribute("uname", username);
            session.setAttribute("pwd", password);
            resp.sendRedirect(req.getContextPath() + "/trips");
            LOG.info("Login successfull!");
        } else {
            model.put("fail", "Wrong username or password!");

            Template template = HandlebarsTemplateFactory.getTemplate("login");
            template.apply(model, resp.getWriter());
            LOG.info("Login failed!");
        }
    }
}
