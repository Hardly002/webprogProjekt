package edu.bbte.idde.kdim1928.web;

import com.github.jknack.handlebars.Template;
import edu.bbte.idde.kdim1928.backend.dao.TripDao;
import edu.bbte.idde.kdim1928.backend.dao.memory.MemTripDao;
import jakarta.servlet.ServletException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/trips")
public class TripServletHandlebars extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TripServletHandlebars.class);
    private TripDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = MemTripDao.getDao();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LOG.info("Request arrived to example servlet");

        Map<String, Object> model = new ConcurrentHashMap<>();
        model.put("trips", dao.findAll());

        Template template = HandlebarsTemplateFactory.getTemplate("index");
        template.apply(model, resp.getWriter());
    }
}