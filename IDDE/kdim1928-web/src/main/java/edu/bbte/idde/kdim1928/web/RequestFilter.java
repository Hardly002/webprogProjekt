package edu.bbte.idde.kdim1928.web;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebFilter("/trips")
public class RequestFilter extends HttpFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestFilter.class);

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpSession session = req.getSession(false);
        if (session == null || session.getAttribute("uname") == null && session.getAttribute("pwd") == null) {
            res.sendRedirect(req.getContextPath() + "/login");
        } else {
            chain.doFilter(req, res);
        }
        LOGGER.info("{}\n{}\n{}",
                req.getMethod(),
                req.getRequestURI(),
                res.getStatus());
    }
}
