import express from 'express';
import morgan from 'morgan';
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import eformidable from 'express-formidable';

import authentication from './routers/authentication.js';
import books from './routers/books.js';
import users from './routers/users.js';

const app = express();
const uploadDir = join(process.cwd(), 'covers');

if (!existsSync(uploadDir)) {
  mkdirSync(uploadDir);
}

app.use(cors({ origin: 'http://localhost:3000', credentials: true }));
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.static('./covers'));
app.use(eformidable({ uploadDir }));
app.use(cookieParser());
app.use('/api/authentication/', authentication);
app.use('/api/books', books);
app.use('/api/users', users);

app.listen(8080, () => {
  console.log('Webserver started');
});
