import { promisify } from 'util';
import crypto from 'crypto';

export const pbkdf2 = promisify(crypto.pbkdf2);

export const hashinfo = {
  saltSize: 16,
  hashSize: 32,
  hashAlgorithm: 'sha512',
  iterations: 1000,
};

export const secret = '1c28d07215544bd1b24faccad6c14a04';
