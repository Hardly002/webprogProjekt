-- Segédállomány, hogy előkészítsünk egy MySQL adatbázist a példaprogramnak.
-- Futtatás konzolról (UNIX rendszeren): 
--     mysql -u root -p <setup.sql

CREATE DATABASE IF NOT EXISTS webprog;

USE webprog;
-- CREATE USER 'webprog'@'localhost' IDENTIFIED BY 'VgJUjBd8';
GRANT ALL PRIVILEGES ON *.* TO 'webprog'@'localhost';
