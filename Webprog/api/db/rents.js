import dbConnection from './initdb.js';

export const addRent = async (userID, bookID) => {
  try {
    const connection = await dbConnection;
    const query = 'INSERT INTO Rents VALUES (default,?,?,?)';
    await connection.execute(query, [userID, bookID, new Date().toISOString()]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const isRentExists = async (userID, bookID) => {
  try {
    const connection = await dbConnection;
    const query = 'SELECT * FROM Rents WHERE userID = ? AND bookID = ?';
    const [result] = await connection.execute(query, [userID, bookID]);
    return result.length > 0;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const deleteRent = async (userID, bookID) => {
  try {
    const connection = await dbConnection;
    const query = 'DELETE FROM Rents WHERE userID = ? AND bookID = ? LIMIT 1';
    await connection.execute(query, [userID, bookID]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};
