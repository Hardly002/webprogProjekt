import dbConnection from './initdb.js';

export const isUserExists = async (userID) => {
  try {
    const connection = await dbConnection;
    const query = 'SELECT * FROM Users WHERE ID = ?';
    const [result] = await connection.execute(query, [userID]);
    return result.length > 0;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getAllUsers = async () => {
  try {
    const connection = await dbConnection;
    const query = 'SELECT * FROM Users';
    const [result] = await connection.execute(query);
    return result;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getUsersByBook = async (bookID) => {
  try {
    const connection = await dbConnection;
    const [userNames] = await connection.execute('SELECT DISTINCT Users.Name FROM Rents JOIN Users ON Users.ID = Rents.userID WHERE bookID = ?', [bookID]);
    return userNames;
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const addUser = async (username, role, password) => {
  try {
    const connection = await dbConnection;
    await connection.execute('INSERT INTO Users VALUES (default,?,?,?,null)', [username, role, password]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const isUserNameExists = async (username) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT 1 FROM Users WHERE name = ?', [username]);
    return result.length > 0;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getHash = async (username) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT password FROM Users WHERE name = ?', [username]);
    return result[0].password;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getRole = async (username) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT role FROM Users WHERE name = ?', [username]);
    return result[0].role;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getUserID = async (username) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT ID FROM Users WHERE name = ?', [username]);
    return result[0].ID;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const deleteUserById = async (userID) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT * FROM Rents WHERE userID = ?', [userID]);
    if (result.length > 0) {
      return false;
    }
    await connection.execute('DELETE FROM Users WHERE ID = ?', [userID]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const changeRole = async (userID, role) => {
  try {
    const connection = await dbConnection;
    await connection.execute('UPDATE Users SET role = ? WHERE ID = ?', [role, userID]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const changePassword = async (hash, newHash) => {
  try {
    const connection = await dbConnection;
    await connection.execute('UPDATE Users SET password = ? WHERE password = ?', [newHash, hash]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getFileName = async (username) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT picture FROM Users WHERE name = ?', [username]);
    return result[0].picture;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const setFileName = async (username, fileName) => {
  try {
    const connection = await dbConnection;
    await connection.execute('UPDATE Users SET picture = ? WHERE name = ?', [fileName, username]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};
