// Adatbázis műveleteket végző modul
import mysql from 'mysql2/promise.js';

async function createDbConnection() {
  try {
    const connection = await mysql.createPool({
      connectionLimit: 10,
      database: 'webprog',
      host: 'localhost',
      port: 3306,
      user: 'webprog',
      password: 'VgJUjBd8',
    });
    console.log('Sikeresen racsatlakoztunk az adatbazisra!');
    const query1 = `CREATE TABLE IF NOT EXISTS Books (ID int AUTO_INCREMENT, isbn varchar(15), name varchar(100), author varchar(50),
      year int, quantity int, description varchar(2000), cover varchar(250), PRIMARY KEY(ID, isbn))`;
    const query2 = 'CREATE TABLE IF NOT EXISTS Rents (ID int AUTO_INCREMENT, userID int, bookID int, date varchar(50), PRIMARY KEY(ID))';
    const query3 = 'CREATE TABLE IF NOT EXISTS Users (ID int NOT NULL AUTO_INCREMENT, name varchar(50), role varchar(20), password varchar(250), PRIMARY KEY (ID))';

    await connection.query(query1);
    console.log('Sikeresen letrehoztuk a tablat(books)!');
    await connection.query(query2);
    console.log('Sikeresen letrehoztuk a tablat!(rents)');
    await connection.query(query3);
    console.log('Sikeresen letrehoztuk a tablat!(Users)');
    return connection;
  } catch (error) {
    console.error(`Hiba tortent a csatlakozaskor: ${error.sqlMessage}`);
    return null;
  }
}

export default createDbConnection();
