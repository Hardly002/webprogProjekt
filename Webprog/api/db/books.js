import dbConnection from './initdb.js';

export const isBookExists = async (bookID) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT * FROM Books WHERE ID = ?', [bookID]);
    return result.length > 0;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getAllBooks = async () => {
  try {
    const connection = await dbConnection;
    const query = 'SELECT * FROM Books';
    const [result] = await connection.execute(query);
    return result;
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const insertBook = async (book, savedFileName) => {
  try {
    const connection = await dbConnection;
    const query = 'INSERT INTO Books VALUES (default,?,?,?,?,?,?,?)';
    await connection.execute(query, [book.isbn, book.bookname, book.author,
      book.year, book.quantity, book.description, savedFileName]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const findBook = async (isbn) => {
  try {
    const connection = await dbConnection;
    const query = 'SELECT ID FROM Books WHERE isbn = ?';
    const [result] = await connection.execute(query, [isbn]);
    return result;
  } catch (error) {
    console.log(error);
    return {};
  }
};

export const updateQuantity = async (isbn, quantity) => {
  try {
    const connection = await dbConnection;
    const query = 'UPDATE Books SET quantity = quantity + ? WHERE isbn = ?';
    await connection.execute(query, [quantity, isbn]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getQuantity = async (bookID) => {
  try {
    const connection = await dbConnection;
    const query = 'SELECT quantity FROM Books WHERE ID = ?';
    const [result] = await connection.execute(query, [bookID]);
    return result[0].quantity;
  } catch (error) {
    console.log(error);
    return -1;
  }
};

export const getBookID = async (isbn) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT ID FROM Books WHERE isbn = ?', [isbn]);
    return result[0].ID;
  } catch (error) {
    console.log(error);
    return -1;
  }
};

export const deleteBookByID = async (id) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT * FROM Rents WHERE bookID = ?', [id]);
    if (result.length > 0) {
      return false;
    }
    await connection.execute('DELETE FROM Books WHERE ID = ?', [id]);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getDescription = async (id) => {
  try {
    const connection = await dbConnection;
    const [description] = await connection.execute('SELECT description FROM Books WHERE ID = ?', [id]);
    return description;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getBookByISBN = async (isbn) => {
  try {
    const connection = await dbConnection;
    const [result] = await connection.execute('SELECT * FROM Books WHERE isbn = ?', [isbn]);
    return result;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getBooksOfUser = async (username) => {
  try {
    const connection = await dbConnection;
    const query = `SELECT books.name FROM rents
      JOIN books on books.ID = bookID
      JOIN users on users.ID = userID
      WHERE users.name = ?;`;
    const [result] = await connection.execute(query, [username]);
    return result;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const getBooksOfUserByID = async (userID) => {
  try {
    const connection = await dbConnection;
    const query = `SELECT books.name FROM rents
      JOIN books on books.ID = bookID
      JOIN users on users.ID = userID
      WHERE users.ID = ?;`;
    const [result] = await connection.execute(query, [userID]);
    return result;
  } catch (error) {
    console.log(error);
    return false;
  }
};
