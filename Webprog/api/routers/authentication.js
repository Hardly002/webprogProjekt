import { Router } from 'express';
import jwt from 'jsonwebtoken';
import crypto from 'crypto';
import {
  getHash,
  getRole, addUser, isUserNameExists,
} from '../db/users.js';
import * as secret from '../secret/secret.js';
import { validateRegist } from '../middleware/validator.js';

const router = Router();

router.post('/login', async (req, resp) => {
  const { username, password } = req.fields;
  const hashWithSalt = await getHash(username);
  if (hashWithSalt) {
    const expectedHash = hashWithSalt.substring(0, secret.hashinfo.hashSize * 2),
      salt = Buffer.from(hashWithSalt.substring(secret.hashinfo.hashSize * 2), 'hex');
    // újra-hash-elés
    const binaryHash = await secret.pbkdf2(
      password,
      salt,
      secret.hashinfo.iterations,
      secret.hashinfo.hashSize,
      secret.hashinfo.hashAlgorithm,
    );
    // hexa stringgé alakítás
    const actualHash = binaryHash.toString('hex');
    if (expectedHash === actualHash) {
      const role = await getRole(username);
      const token = jwt.sign({ username, role }, secret.secret);
      resp.cookie('token', token, { httpOnly: false });
      resp.status(200).send(role);
    } else {
      resp.status(202).send('Incorrect password');
    }
  } else {
    resp.status(202).send('This username not exists!');
  }
});

router.post('/regist', validateRegist(), async (req, resp) => {
  const { username, password, role } = req.fields;
  if (!await isUserNameExists(username)) {
    const salt = crypto.randomBytes(secret.hashinfo.saltSize);
    const hash = await secret.pbkdf2(
      password,
      salt,
      secret.hashinfo.iterations,
      secret.hashinfo.hashSize,
      secret.hashinfo.hashAlgorithm,
    );
    const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
    // console.log(username);
    // console.log(role);
    const res = await addUser(username, role, hashWithSalt);
    if (!res) {
      resp.status(202).send('Error while adding user!');
    } else {
      resp.status(200).send('Registration successfull!');
    }
  } else {
    resp.status(202).send('Username Taken!');
  }
});

export default router;
