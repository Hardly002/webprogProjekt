import { Router } from 'express';
import { basename } from 'path';

import * as booksDb from '../db/books.js';
import authorize from '../middleware/authorize.js';
import { getUserID, getUsersByBook } from '../db/users.js';
import { addRent, deleteRent, isRentExists } from '../db/rents.js';

import {
  addBookValidator, isBookExistsByISBN,
  isBookExistsByID, validateRentOrRent,
} from '../middleware/validator.js';

const router = Router();

async function insertBook(newBook, savedFileName) {
  const result = await booksDb.insertBook(newBook, savedFileName);
  if (result) {
    console.log('Book added!');
    return true;
  }
  console.log('There was an error while adding book!');
  return false;
}

async function updateQuantity(newBook) {
  const result = await booksDb.updateQuantity(newBook.isbn, newBook.quantity);
  if (result) {
    console.log('Quantity updated!');
    return true;
  }
  console.log('There was an error while updating quantity!');
  return false;
}

// --------------------  Get All Books ------------------------ //
router.get('/', async (req, resp) => {
  const books = await booksDb.getAllBooks();
  resp.end(JSON.stringify(books));
});

// --------------------  Delete Book ------------------------ //
router.delete('/:bookID', authorize(['admin']), async (req, resp) => {
  const { bookID } = req.params;
  const success = await booksDb.deleteBookByID(bookID);
  if (success) {
    resp.status(200).send('Successfully deleted!');
  } else {
    resp.status(202).send('It is not possible to delete rented books!');
  }
});

// ----------------------  Add Book -------------------------- //
router.post('/addbooks', authorize(['admin']), addBookValidator(), isBookExistsByISBN(), async (req, resp) => {
  const newBook = req.fields;
  if (!resp.locals.isBookExists) {
    if (!await insertBook(newBook, basename(req.files.cover.path))) {
      resp.status(422).send('There was an error while adding book!');
    } else {
      resp.status(200).send('Successfully added!');
    }
  } else if (!await updateQuantity(newBook)) {
    resp.status(422).send('There was an error while updating quantity!');
  } else {
    resp.status(200).send('Successfully updated quantity!');
  }
});

// -----------------------  Rent Book --------------------------- //
router.post('/rentbooks', authorize(), validateRentOrRent(), isBookExistsByID(), async (req, resp) => {
  console.log('hello');
  const rent = req.fields;
  const userID = await getUserID(resp.locals.jwt.username);
  if (resp.locals.isBookExists) {
    if (await isRentExists(userID, rent.bookID)) {
      console.log('can not rent beacuse already rented!');
      resp.status(202).send('You already rented this book!');
    } else {
      const quantity = await booksDb.getQuantity(rent.bookID);
      if (quantity > 0) {
        await booksDb.updateQuantity(rent.bookID, -1);
        await addRent(userID, rent.bookID);
        console.log('Somebody rented a book');
        resp.status(200).send('Seuccessfully rented!');
      } else {
        resp.status(202).send('There is no enough book for rent!');
        console.log('There is no enough book for rent!');
      }
    }
  } else {
    resp.status(202).send('Book not found!');
    console.log('Book Not Found');
  }
});

// ---------------------- Retrun Book  -------------------------- //
router.post('/returnbooks', authorize(), validateRentOrRent(), isBookExistsByID(), async (req, resp) => {
  const ret = req.fields;
  if (resp.locals.isBookExists) {
    const userID = await getUserID(resp.locals.jwt.username);
    const isRent = await isRentExists(userID, ret.bookID);
    if (isRent) {
      await deleteRent(userID, ret.bookID);
      await booksDb.updateQuantity(ret.bookID, 1);
      resp.status(200).send('Seuccessfully returned!');
      console.log('successfully returned a book!');
    } else {
      resp.status(202).send('You have not rented this book!');
      console.log('have not rented this book!');
    }
  } else {
    resp.status(202).send('This book not exists!');
    console.log('This book is not exists!');
  }
});

// ----------------------- Get Description ------------------------- //
router.get('/:bookID/description', async (req, resp) => {
  const { bookID } = req.params;
  const description = await booksDb.getDescription(bookID);
  resp.end(JSON.stringify(description[0].description));
});

// ----------------------- Get Rents of a Book ---------------------- //
router.get('/rentsofbooks/:bookID', authorize(['admin']), async (req, resp) => {
  const userNames = await getUsersByBook(req.params.bookID);
  resp.end(JSON.stringify(userNames));
});

export default router;
