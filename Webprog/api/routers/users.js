import Router from 'express';
import crypto from 'crypto';
import { basename } from 'path';

import { getBooksOfUser, getBooksOfUserByID } from '../db/books.js';
import {
  getAllUsers, deleteUserById,
  isUserExists, changeRole,
  getHash,
  changePassword, getFileName, setFileName,
} from '../db/users.js';
import * as secret from '../secret/secret.js';
import authorize from '../middleware/authorize.js';

const router = Router();

// ---------------------- Get All User ------------------------------ //
router.get('/', authorize(['admin']), async (req, resp) => {
  const users = await getAllUsers();
  resp.end(JSON.stringify(users));
});

// ----------------------- Get Rents for User -------------------------- //
router.get('/myrents', authorize(), async (req, resp) => {
  const bookNames = await getBooksOfUser(resp.locals.jwt.username);
  resp.end(JSON.stringify(bookNames));
});

// ------------------------ Delete User ----------------------------- //
router.delete('/:userID', authorize(['admin']), async (req, resp) => {
  const { userID } = req.params;
  const success = await deleteUserById(userID);
  if (success) {
    resp.status(200).send('Successfully deleted!');
  } else {
    resp.status(202).send('It is not possible to delete this user, because he isn\'t returned every book!');
  }
});

// ------------------------ Change Role -------------------------------- //
router.post('/changeroles', authorize(['admin']), async (req, resp) => {
  const user = req.fields;
  const roles = ['admin', 'user'];
  if (await isUserExists(user.userID)) {
    if (roles.includes(user.role)) {
      if (await changeRole(user.userID, user.role)) {
        resp.status(200).send(`Role changed to ${user.role.toUpperCase()}`);
      } else {
        resp.status(500).send('Error');
      }
    } else {
      resp.status(422).send('Role not exists!');
    }
  } else {
    resp.status(404).send('This user not exists!');
  }
});

// ------------------------- Get Rents Of User -------------------------- //
router.get('/:userID/rents', authorize(['admin']), async (req, resp) => {
  const { userID } = req.params;
  console.log(userID);
  const booknames = await getBooksOfUserByID(userID);
  resp.send(JSON.stringify(booknames));
});

router.post('/changepassword', authorize(), async (req, resp) => {
  const { oldpassword, newpassword1, newpassword2 } = req.fields;
  if (newpassword1 !== newpassword2) {
    resp.status(202).send('Passwords do not match!');
    return;
  }
  const hashWithSalt = await getHash(resp.locals.jwt.username);
  const expectedHash = hashWithSalt.substring(0, secret.hashinfo.hashSize * 2);
  const salt = Buffer.from(hashWithSalt.substring(secret.hashinfo.hashSize * 2), 'hex');
  // újra-hash-elés
  const binaryHash = await secret.pbkdf2(
    oldpassword,
    salt,
    secret.hashinfo.iterations,
    secret.hashinfo.hashSize,
    secret.hashinfo.hashAlgorithm,
  );
  // hexa stringgé alakítás
  const actualHash = binaryHash.toString('hex');
  if (expectedHash === actualHash) {
    const salt2 = crypto.randomBytes(secret.hashinfo.saltSize);
    const hash = await secret.pbkdf2(
      newpassword1,
      salt2,
      secret.hashinfo.iterations,
      secret.hashinfo.hashSize,
      secret.hashinfo.hashAlgorithm,
    );
    const newHashWithSalt = Buffer.concat([hash, salt2]).toString('hex');
    if (await changePassword(hashWithSalt, newHashWithSalt)) {
      resp.status(200).send('Password successfully changed!');
    } else {
      resp.status(202).send('Error');
    }
  } else {
    resp.status(202).send('Old password is Incorrect');
  }
});

// ------------------- Get Profile Pictrue ------------------- //
router.get('/picture', authorize(), async (req, resp) => {
  const fileName = await getFileName(resp.locals.jwt.username);
  resp.status(200).send(fileName);
});

router.post('/picture', authorize(), async (req, resp) => {
  if (await setFileName(resp.locals.jwt.username, basename(req.files.picture.path))) {
    resp.status(200).send('Profile picture successfully changed!');
  } else {
    resp.status(202).send('Error! Please try again later!');
  }
});

export default router;
