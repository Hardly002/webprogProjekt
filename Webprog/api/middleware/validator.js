import joi from 'joi';
import { createValidator } from 'express-joi-validation';

import { getBookByISBN, isBookExists } from '../db/books.js';

const addBookSchema = joi.object({
  isbn: joi.string().length(13).pattern(/^[0-9]+$/).required(),
  bookname: joi.string().min(3).max(100),
  author: joi.string().min(3).max(30),
  year: joi.number().integer().min(1970).max(new Date().getFullYear()),
  quantity: joi.number().integer().min(1).max(100),
  description: joi.string().min(100).max(2500).required(),
  cover: joi.string().min(5).pattern(/^[a-zA-z0-9]+[.](jpeg|jpg|png)$/),
});

const method = (value, helpers) => {
  if (value !== 'admin' && value !== 'user') {
    return helpers.error('any.ivalid');
  }

  return value;
};

const registSchema = joi.object({
  username: joi.string().min(5).max(30),
  password: joi.string().min(8),
  role: joi.string().custom(method, 'Role not Exists!'),
});

export function addBookValidator() {
  return createValidator().fields(addBookSchema);
}

export function isBookExistsByISBN() {
  return async (req, res, next) => {
    const isbook = await getBookByISBN(req.fields.isbn);
    console.log(isbook.length);
    res.locals.isBookExists = isbook.length > 0;
    next();
  };
}

export function isBookExistsByID() {
  return async (req, res, next) => {
    res.locals.isBookExists = await isBookExists(req.fields.bookID);
    next();
  };
}

export function validateRentOrRent() {
  return (req, res, next) => {
    if (Object.keys(req.fields).length !== 1) {
      res.status(202).send('Please choose a book!');
    }
    next();
  };
}

export function validateRegist() {
  return createValidator().fields(registSchema);
}
