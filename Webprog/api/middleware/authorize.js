import jwt from 'jsonwebtoken';
import { secret } from '../secret/secret.js';

export default function authorize(roles = ['user', 'admin']) {
  return async (req, res, next) => {
    if (req.cookies.token) {
      try {
        const result = await jwt.verify(req.cookies.token, secret);
        res.locals.jwt = result;
        if (!result) {
          // a felhasználó nincs bejelentkezve
          res.status(401).send('You are not logged in');
        } else if (!roles.includes(result.role)) {
          // a felhasználó be van jelentkezve de nincs joga ehhez az operációhoz
          res.status(401).send('You do not have permission to access this endpoint');
        } else {
          // minden rendben
          next();
        }
      } catch (error) {
        console.log(error);
        res.status(401).send('You are not logged in');
      }
    } else {
      res.status(401).send('no cookie');
    }
  };
}
