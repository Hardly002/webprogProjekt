import { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Settings(props) {
    const [show, setShow] = useState(props.show);
    const navigate = useNavigate();

    useEffect(()=>{setShow(props.show)},[props.show]);

    const handleClickPass = () => {
        setShow(false);
        navigate('/users/changepassword', { replace: true });
    }

    const handleClickPic = () => {
        setShow(false);
        navigate('/users/changeprofilepicture', { replace: true });
    }

    if (!show){
        return <></>;
    }

    return (
        <div className="settings">
            <button onClick={handleClickPass}>Change Password</button>
            <button onClick={handleClickPic}>Change profile picture</button>
        </div>
    );
}