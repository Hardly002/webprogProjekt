import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AddBook from './AddBook';
// import { useCookies } from 'react-cookie';

import Authentication from './Authentication';
import Header from './Header';
import Home from './Home';
import NavBar from './NavBar';
import RentOrReturnBook from './RentOrReturnBook';
import RentsOfBookOrMyRents from './RentsOfBookOrMyRents';
import Users from './Users';
import UserRents from './UserRents'
import ChangePassword from './ChangePassword';
import ChangeProfilePicture from './ChangeProfilePicture'

function App() {

  return (
    <BrowserRouter>
      {/* navbar minden oldal tetején */}

      <Header/>
      <NavBar/>

      {/* főoldal route-ok szerint a 2 között */}
      <main>
        <Routes>
          <Route exact path='/' element={<Home/>} />
          <Route exact path='/authentication/login' element={<Authentication show='login'/>} />
          <Route exact path='/authentication/regist' element={<Authentication show='regist'/>} />
          <Route exact path='/book/addbook' element={<AddBook/>} />
          <Route exact path='/book/rentbook' element={<RentOrReturnBook isRent={true} /> } />
          <Route exact path='/book/returnbook' element={<RentOrReturnBook isRent={false} /> } />
          <Route exact path='/book/myrents' element={<RentsOfBookOrMyRents isRentsOfBook={false} />}/>
          <Route path='/book/rentsofbook/:bookId' element={<RentsOfBookOrMyRents isRentsOfBook={true} />}/>
          <Route exact path='/users' element={<Users/>}/>
          <Route path='/users/rentsofusers/:userId' element={<UserRents/>}/>
          <Route exact path='/users/changepassword' element={<ChangePassword />}/>
          <Route exact path='/users/changeprofilepicture' element={<ChangeProfilePicture />}/>
          <Route path='*' element={<i>Cannot find page</i>} /> 
        </Routes>
      </main>
    </BrowserRouter>
  );
}

export default App;
