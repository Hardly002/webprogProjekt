import { useState } from "react";

import Message from "./Message";
import { fetchChangePassword } from "../service/users";

export default function ChangePassword() {
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword1, setNewPassword1] = useState('');
    const [newPassword2, setNewPassword2] = useState('');

    const [message, setMessage] = useState('');
    const [messageOk, setMessageOk] = useState(false);
    
    const checkPassEq = () => {
        if (newPassword1 === newPassword2) {
            setMessageOk(true)
            setMessage('');
            return true;
        } else {
            setMessageOk(false);
            setMessage('Passwords do not match!');
            return false;
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        if (checkPassEq()) {
            if (newPassword1.length >= 8 && newPassword2.length >= 8) {
                const form = new FormData();
                form.append("oldpassword", oldPassword);
                form.append("newpassword1", newPassword1);
                form.append("newpassword2", newPassword2);
                const [status, msg] = await fetchChangePassword(form);
                status === 200 ? setMessageOk(true) : setMessageOk(false);
                setMessage(msg);
                if (status === 200) {
                    setOldPassword('');
                    setNewPassword1('');
                    setNewPassword2('');
                } 
            } else {
                setMessageOk(false);
                setMessage('New password length min 8 character!');
            }
        } 
    }

    return (
        <div className="form">
            <form name="passwordChange" onSubmit={handleSubmit}>
                <h1>Change Password</h1>
                <input type="password" placeholder="Old Password" autoComplete="current-password" value={oldPassword} onChange={(e)=>setOldPassword(e.target.value)}/>
                <input type="password" placeholder="New Password" autoComplete="new-password" value={newPassword1} onChange={(e)=>setNewPassword1(e.target.value)}/>
                <input type="password" placeholder="New Password" autoComplete="new-password" value={newPassword2} onChange={(e)=>setNewPassword2(e.target.value)}
                    onBlur={checkPassEq}/>
                <button type="submit" >Change</button>
            </form>
            <Message ok={messageOk} message={message} />
        </div>
    );
}