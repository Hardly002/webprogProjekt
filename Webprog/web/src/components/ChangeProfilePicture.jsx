import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { changeProfilePicture } from "../service/users";
import Message from "./Message";

export default function ChangeProfilePicture() {
    const [file, setFile] = useState({ name: ''});
    const [message, setMessage] = useState('');
    const [messageOk, setMessageOk] = useState(false);  

    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (file.name === '') {
            setMessageOk(false);
            setMessage('Please select picture!');
        } else {
            const form = new FormData();
            form.append("picture", file);
            const [status, msg] = await changeProfilePicture(form);
            
            setMessage(msg);

            if (status === 200) {
                setMessageOk(true);
                setTimeout(()=>{
                    navigate('/', { replace: true });
                }, 2000);
            } else {
                setMessageOk(false);
            }
        }
    }

    return (
        <div className="form">
            <form name="changeProfilepicture" onSubmit={handleSubmit}>
                <h1>Upload Profile Picture</h1>
                <label htmlFor="cover">
                        Upload<br/>
                        <img src={"/image.png"} alt="fileIcon"/>
                        <input type="file" id="cover" name="cover" onChange={(e) => {setFile(e.target.files[0])}}/><br/>
                        <span id="imageName">
                            <p>{file.name}</p>
                        </span>
                    </label><br/>
                <button type="submit" >Change</button>
            </form>
            <Message ok={messageOk} message={message} />
        </div>
    );
}