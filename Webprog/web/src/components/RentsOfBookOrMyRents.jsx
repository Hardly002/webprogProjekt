import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { fetchRentsOfBook } from "../service/book";
import { fetchMyRents } from "../service/users";

export default function RentsOfBookOrMyRents(props) {
    const [title, setTitle] = useState('');
    const [list, setList] = useState([]);    

    const { bookId } = useParams();

    useEffect(()=>{
        async function fetchData() {
            if (props.isRentsOfBook) {
                setList(await fetchRentsOfBook(bookId));
                setTitle('Users who rented this book:');
            } else {
                setList(await fetchMyRents());
                setTitle('My rented books:');
            }
        }
        fetchData();
    },[props.isRentsOfBook, bookId]);

    return (
        <div className="users">
            <h2>{title}</h2>
            {list.map((obj, index)=>(
                <div key={index} className="user">
                    <p>{obj}</p>
                </div>
            ))}
        </div>
    );
}