import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";

import { deleteUserById, changeRole } from "../service/users";
import { fetchImage } from "../service/book";
import Message from "./Message";

export default function BookListEntry({ user }) {
    const [deleted, setDeleted] = useState(false);
    const [role, setRole] = useState(user.role);
    const [img, setImg] = useState();
    const [message, setMessage] = useState('');
    const [messageOk, setMessageOk] = useState(false);

    const [cookies,] = useCookies('name');

    const navigate = useNavigate();

    useEffect(()=>{
        async function fetchImages () {
            if (user.picture !== null){
                const imageObjectURL = await fetchImage(user.picture);
                setImg(imageObjectURL);
            } else {
                setImg("/userlogo.png");
            }
        };

        fetchImages();
    },[user.picture]);

    useEffect(()=>{
        cookies.name.toLowerCase() === user.name.toLowerCase() ? setDeleted(true) : setDeleted (false);
    },[user, cookies])

    const onDeleteUser = async () => {
        try {
            const [status, msg] = await deleteUserById(user.ID);
            status === 200 ? setMessageOk(true) : setMessageOk(false);
            setMessage(msg);
            setTimeout(()=>{
                status === 200 ? setDeleted(true) : setDeleted(false);
                setMessage('');
            },3000);
        } catch (err) {
            console.log(err);
        }

    }

    const onRentsButton = () => {
        navigate(`/users/rentsofusers/${user.ID}`, { replace: true });
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const form = new FormData();
            form.append("userID", user.ID);
            form.append("role", role);
            const [status, msg] = await changeRole(form);
            status === 200 ? setMessageOk(true) : setMessageOk(false);
            setMessage(msg);
            setTimeout(()=>{
                user.role = role;
                setMessage('');
            },3000);
        } catch (error){
            console.log(error);
        }
    }

    if (deleted) {
        return <></>;
    }

    return (
        <>
            <div id={user.ID} className="column">
                <div id="cover" className="cover">
                    <img src={img} alt="kep" />
                </div>
                <div className="info1">
                    <div className="info2">
                        <span className="bookname">{user.name}</span>
                        <span className="role">{user.role}</span>
                        <span className="changeRole">
                            <form name="roleChange" onSubmit={handleSubmit}>
                                <select name="role" value={role} onChange={(e)=>setRole(e.target.value)}>
                                    <option value="user">User</option>
                                    <option value="admin">Admin</option>
                                </select>
                                <button id="submit" type="sumbit">Change</button>
                            </form>
                        </span>
                    </div>
                    <div className="options">
                        <button id="rentsButton" onClick={onRentsButton}>Rents</button>
                    </div>
                    <div className="options">
                        <button id="deleteButton" onClick={onDeleteUser}>Delete</button>
                    </div>
                </div>
            </div>
            { message && (
                <div id="deleteResult" className="deleteResult">
                    <Message ok={messageOk} message={message} />
                </div>
            )}
        </>
    );
}