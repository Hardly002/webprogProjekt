import { useEffect } from "react";
import { useState } from "react";
import { fetchAllBooks, fetchRentBook, fetchReturnBook } from "../service/book";
import Message from "./Message";


export default function RentOrReturnBook(props) {
    const [books, setBooks] = useState([]);
    const [selectedBookId, setSelectedBookId] = useState('');
    const [title, setTitle] = useState('');
    const [buttonText, setButtonText] = useState('');
    const [message, setMessage] = useState('');
    const [messageOk, setMessageOk] = useState(false);

    useEffect(()=>{
        async function fetchData() {
            setBooks(await fetchAllBooks());
        }
        fetchData();
    },[]);

    useEffect(() => {
        setSelectedBookId('');
        setMessage('');
        if (props.isRent) {
            setTitle('Rent a Book');
            setButtonText('Rent');
        } else {
            setTitle('Renturn a Book');
            setButtonText('Return');
        }
    },[props.isRent]);

    // useEffect(()=>{console.log(selectedBookId)},[selectedBookId]);


    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const form = new FormData();
            form.append("bookID", selectedBookId);
            let [status, msg] = [0,''];
            if (props.isRent){
                [status, msg] = await fetchRentBook(form);
            }else {
                [status, msg] = await fetchReturnBook(form);
            }
            status === 200 ? setMessageOk(true) : setMessageOk(false);
            setMessage(msg);
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div className="form">
            <h1>{title}</h1>
            <form id="bookform" onSubmit={handleSubmit}>
                <select name="bookID" id="bookID" value={selectedBookId} onChange={(e)=>setSelectedBookId(e.target.value)}>
                    <option id={-1} value="" disabled>Select book</option>
                    {books.map((book) => (
                        <option key={book.ID} value={book.ID}>{book.name}</option>
                    ))}
                </select><br/>
                <button type="submit" id="submit" name="submit">{buttonText}</button>
            </form>
        <Message ok={messageOk} message={message} />
        </div>
    );

}