import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { fetchUserRents } from "../service/users";

export default function RentsOfBookOrMyRents(props) {
    const [title, setTitle] = useState('');
    const [list, setList] = useState([]);    

    const { userId } = useParams();

    useEffect(()=>{
        async function fetchData() {
            setTitle('Rents:');
            setList(await fetchUserRents(userId))
        }
        fetchData();
    },[userId]);

    return (
        <div className="users">
            <h2>{title}</h2>
            {list.map((obj, index)=>(
                <div key={index} className="user">
                    <p>{obj}</p>
                </div>
            ))}
        </div>
    );
}