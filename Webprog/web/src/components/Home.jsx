import { useState, useEffect } from 'react';
import { fetchAllBooks } from '../service/book';
import BookListEntry from './BookListEntry';

export default function Home() {
    const [books, setBooks] = useState([]);
    const [search, setSearch] = useState('');
    const [radio, setRadio] = useState(false);
    const [placeholder, setPlaceholder] = useState('');

    useEffect(() => {
        fetchAllBooks().then(setBooks).then();
    }, []);

    useEffect(()=>{
        if (!radio) {
            setPlaceholder('Search by book name...');
        } else {
            setPlaceholder('Search by author...');
        }
    },[radio]);

    return (
        <div className='konyvek'>
            <div className='search'>
                <input type="text" placeholder={placeholder} value={search} onChange={(e)=>setSearch(e.target.value)}/>
                <input type="radio" name="radio" defaultChecked onClick={(e)=>setRadio(false)}/>
                <input type="radio" name="radio" onClick={(e)=>setRadio(true)}/>
            </div>
            {books.map((book) => {
                if (search === '' || (!radio && book.name.startsWith(search)) || (radio && book.author.startsWith(search))){
                    return (<BookListEntry key={book.ID} book={book} />)
                }
                return undefined;
            })}
        </div>
    );
}