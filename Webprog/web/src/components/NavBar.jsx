import { NavLink } from 'react-router-dom';
import { useCookies } from 'react-cookie';
import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

export default function NavBar() {
    const [cookies, ] = useCookies(['token', 'isAdmin']);
    const location = useLocation();

    useEffect(()=>{}, [cookies]);

    useEffect(()=>{

    },[location]);

    return (
        <div className="menu">
            <ul>
                <li><NavLink to="/" activeclassname="active">Home</NavLink></li>
                { cookies?.token && (
                <>
                    <li><NavLink activeclassname="active" to="/book/rentbook">Rent</NavLink></li>
                    <li><NavLink activeclassname="active" to="/book/returnbook">Return</NavLink></li>
                    <li><NavLink activeclassname="active" to="/book/myrents">My Rents</NavLink></li>
                </>
                )}
                { cookies?.token && cookies.isAdmin === 'true' && (
                <>
                    <li><NavLink activeclassname="active" to="/book/addbook">Add</NavLink></li>
                    <li><NavLink activeclassname="active" to="/users">Users</NavLink></li>
                </>
                )}
            </ul>
        </div>
    );
}
