import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { fetchLogin, fetchRegist } from "../service/users";
import Message from "./Message";

export default function Authentication(props) {

    const navigate = useNavigate();
    const location = useLocation();
    const from = location.state?.from?.pathname || '/';

    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('');
    const [message, setMessage] = useState('');
    const [messageOk, setMessageOk] = useState(false);

    const [, setCookie] = useCookies(['token']);


    function handleNameChange(event) {
        setName(event.target.value);
    }
    function handlePasswordChange(event) {
        setPassword(event.target.value);
    }
    
    const handleRegist = async (e) => {
        e.preventDefault();
        try {
            const form = new FormData();
            form.append("username", name);
            form.append("password", password);
            form.append("role", role);
            const [status, msg] = await fetchRegist(form);
            status === 200 ? setMessageOk(true) : setMessageOk(false);
            setMessage(msg);
            if (status === 200) {
                setName('');
                setPassword('');
                setRole('');
            } 
        } catch (error) {
            console.log(error);
            setMessage(error.response.data);
            setMessageOk(false);
        }
    }

    const handleLogin = async (e) => {
        e.preventDefault();
        try {
            const form = new FormData();
            form.append("username", name);
            form.append("password", password);

            const [status, userRole] = await fetchLogin(form);
            if (status === 200) {
                setCookie('name', name);
                setCookie('isAdmin', userRole === 'admin');
                navigate(from, { replace: true });
            } else {
                setMessage(userRole);   
                setMessageOk(false);
            }
        } catch (error) {
            console.log(error);
            setMessage(error.response.data);
            setMessageOk(false);
        }
    }

    useEffect(() => {
        setName('');
        setPassword('');
        setRole('');
        setMessage('');
    }, [props]);

    if (props.show === 'login') {
        return (
            <div className="form">
                <h1>Log in</h1>
                <form id="loginform" onSubmit={handleLogin}>
                    <input type="text" id="username" name="username" placeholder="Username" value={name} onChange={handleNameChange} /><br />
                    <input type="password" id="password" name="password" placeholder="Password" value={password} onChange={handlePasswordChange} /><br />
                    <Link to="/authentication/regist">Registration</Link><br />
                    <button type="submit" id="submit" name="submit">Log In</button>
                </form>
                <p className="notok">{message}</p>
            </div>
        );
    }
    return (
        <div className="form">
            <h1>Registration</h1>
            <form id="loginform" onSubmit={handleRegist}>
                <input type="text" id="username" name="username" placeholder="Username" value={name} onChange={handleNameChange} /><br />
                <input type="password" id="password" name="password" placeholder="Password" value={password} onChange={handlePasswordChange} /><br />
                <select name="role" className="select" value={role} onChange={(e)=>setRole(e.target.value)}>
                    <option value={''} disabled>Select Role</option>
                    <option value={'user'} >user</option>
                    <option value={'admin'}>admin</option>
                </select><br/>
                <Link to="/authentication/login">Log In</Link><br />
                <button type="submit" id="submit" name="submit">Regist</button>
            </form>
            <Message ok={messageOk} message={message}/>
        </div>
    );
}