import { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

import { fetchAddBook } from "../service/book";
import Message from "./Message";

export default function AddBook (props) {
    const [isbn, setIsbn] = useState('');
    const [bookName, setBookName] = useState('');
    const [author, setAuthor] = useState('');
    const [year, setYear] = useState('');
    const [quantity, setQuantity] = useState('');
    const [description, setDescription] = useState('');
    const [file, setFile] = useState({ name: ''});
    const [message, setMessage] = useState('');
    const [messageOk, setMessageOk] = useState(false);


    const navigate = useNavigate();

    useEffect(()=>{setMessage('');setMessageOk(false)},[]);

    const onBlurISBN = (e) => {
        if (e.target.value.length !== 13) {
            setMessage('ISBN length must be equal to 13!');
            setMessageOk('false');
        } else {
            setMessage('');
        }
    }

    const onBlurBookName = (e) => {
        if (e.target.value.length < 3 || e.target.value.length > 100) {
            setMessage('Book Name min 3 character and max 100');
            setMessageOk('false');
        } else {
            setMessage('');
        }
    }

    const onBlurAuthor = (e) => {
        if (e.target.value.length < 3 || e.target.value.length > 30) {
            setMessage('Author min 3 character');
            setMessageOk('false');
        } else {
            setMessage('');
        }
    }

    const onBlurYear = (e) => {
        if (e.target.value < 1970 || e.target.value > new Date().getFullYear()) {
            setMessage('Year must be between 1970 & 2022');
            setMessageOk('false');
        } else {
            setMessage('');
        }
    }

    const onBlurQuantity = (e) => {
        if (e.target.value < 1 || e.target.value > 100) {
            setMessage('Quantity must be between 1 & 100');
            setMessageOk('false');
        } else {
            setMessage('');
        }
    }

    const onBlurDescription = (e) => {
        if (e.target.value.length < 100) {
            setMessage('Description min 100 character');
            setMessageOk('false');
        } else {
            setMessage('');
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const form = new FormData();
            form.append("isbn", isbn);
            form.append("bookname", bookName);
            form.append("author", author);
            form.append("year", year);
            form.append("quantity", quantity);
            form.append("description", description);
            form.append("cover", file);

            const [status, msg] = await fetchAddBook(form);
            
            setMessage(msg);

            if (status === 200) {
                setMessageOk(true);
                setTimeout(()=>{
                    navigate('/', { replace: true });
                }, 2000);
            } else {
                setMessageOk(false);
            }
        } catch (err){
            console.log(err);
            setMessage(err.response.data);
            setMessageOk(false);
        }
    };

    return (
        <div className="form">
                <h1>Book Registration</h1>
                <form id="addbooks" onSubmit={handleSubmit}>
                    <input type="text" id="isbn" name="isbn" placeholder="ISBN" value={isbn} onChange={(e) => setIsbn(e.target.value)} onBlur={onBlurISBN}/><br/>
                    <input type="text" id="bookname" name="bookname" placeholder="Book Name" value={bookName} onChange={(e) => setBookName(e.target.value)}
                        onBlur={onBlurBookName}/><br/>
                    <input type="text" id="author" name="author" placeholder="Author" value={author} onChange={(e) => setAuthor(e.target.value)}
                        onBlur={onBlurAuthor}/><br/>
                    <input type="number" id="year" name="year" placeholder="Year" value={year} onChange={(e) => setYear(e.target.value)} onBlur={onBlurYear}/><br/>
                    <input type="number" id="quantity" name="quantity" placeholder="Quantity" value={quantity} onChange={(e) => setQuantity(e.target.value)}
                        onBlur={onBlurQuantity}/><br/>
                    <textarea id="description" name="description" placeholder="Description" value={description} onChange={(e) => setDescription(e.target.value)}
                        onBlur={onBlurDescription}></textarea><br/>
                    <label htmlFor="cover">
                        Select Cover<br/>
                        <img src={"/image.png"} alt="fileIcon"/>
                        <input type="file" id="cover" name="cover" onChange={(e) => {setFile(e.target.files[0])}}/><br/>
                        <span id="imageName">
                            <p>{file.name}</p>
                        </span>
                    </label><br/>
                    <button type="submit" id="submit" name="submit">Add Book</button>
                    <Message ok={messageOk} message={message} />
                </form>
            </div>
    );
}