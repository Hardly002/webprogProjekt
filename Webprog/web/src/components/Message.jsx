export default function Message(props) {
    if (props.ok === true) {
        return (
            <p className="ok">{props.message}</p>
        );
    }

    return (
        <p className="notok">{props.message}</p>
    );
}