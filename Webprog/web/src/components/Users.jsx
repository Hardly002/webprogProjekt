import { useEffect, useState } from "react";

import { fetchAllUsers } from "../service/users";
import UserListEntry from './UserListEntry'

export default function Users() {
    const [users, setUsers] = useState([]);
    const [search, setSearch] = useState('');
    const [radio, setRadio] = useState(false);
    const [placeholder, setPlaceholder] = useState('');


    useEffect(()=>{
        fetchAllUsers().then(setUsers).then();
    },[]);

    useEffect(()=>{
        if (!radio) {
            setPlaceholder('Search by username...');
        } else {
            setPlaceholder('Search by role...');
        }
    },[radio]);
    
    return (
        <div className='konyvek'>
            <div className='search'>
                <input type="text" placeholder={placeholder} value={search} onChange={(e)=>setSearch(e.target.value)}/>
                <input type="radio" name="radio" defaultChecked onClick={()=>setRadio(false)}/>
                <input type="radio" name="radio" onClick={()=>setRadio(true)}/>
            </div>
            {users.map((user) => {
                if (search === '' || (!radio && user.name.startsWith(search)) || (radio && user.role.startsWith(search))){
                    return (<UserListEntry key={user.ID} user={user} />);
                }
                return undefined;                
            })}
        </div>
    );
}