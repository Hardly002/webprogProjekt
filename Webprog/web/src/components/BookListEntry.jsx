import { useState, useEffect } from 'react';
import { deleteBookById, fetchImage } from '../service/book';
import { useCookies } from 'react-cookie';
import Message from './Message';
import { useNavigate } from 'react-router-dom';

export default function BookListEntry({ book }) {
    const [deleted, setDeleted] = useState(false);
    const [img, setImg] = useState();
    const [showMore, setShowMore] = useState(false);
    const [message, setMessage] = useState('');
    const [messageOk, setMessageOk] = useState(false);


    const [cookies, ] = useCookies(['token', 'isAdmin'])

    const navigate = useNavigate();

    useEffect(() => {
        async function fetchImages () {
            const imageObjectURL = await fetchImage(book.cover);
            setImg(imageObjectURL);
        };

        fetchImages();
    }, [book.cover]);

    const onDeleteBook = async () => {
        try {
            const [status, msg] = await deleteBookById(book.ID);
            status === 200 ? setMessageOk(true) : setMessageOk(false);
            setMessage(msg);
            setTimeout(()=>{
                status === 200 ? setDeleted(true) : setDeleted(false);
                setMessage('');
            },3000);
        } catch (err) {
            console.log(err);
        }
    }

    const onRentsButton = () => {
        navigate(`/book/rentsofbook/${book.ID}`, { replace: true });
    }

    if (deleted) {
        return <></>;
    }

    return (
        <>
            <div id={book.ID} className="column">
                <div id="cover" className="cover" onClick={()=>{setShowMore(true)}}>
                    <img src={img} alt="kep" />
                </div>
                { showMore && (
                    <div className='moreInfo'>
                        <button className='showLess' onClick={()=>{setShowMore(false)}}>{'\u2715'}</button>
                        <p>{book.description}</p>
                    </div>
                )}
                <div className="info1">
                    <div className="info2" onClick={()=>{setShowMore(true)}}>
                        <span className="author">{book.author}</span>
                        <span className="bookname">{book.name}</span>
                        <span className="description">{book.description.slice(0,350).concat('...')}</span>
                    </div>
                    { cookies?.token && cookies.isAdmin === 'true' && (
                    <>
                        <div className="options">
                            <button id="rentsButton" onClick={onRentsButton}>Rents</button>
                        </div>
                        <div className="options">
                            <button id="deleteButton" onClick={onDeleteBook}>Delete</button>
                        </div>
                    </>
                    )}
                </div>
            </div>
            { message && (
                <div id="deleteResult" className="deleteResult">
                    <Message ok={messageOk} message={message} />
                </div>
            )}
        </>
    );
}
