import { useState, useEffect } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import { fetchImage } from "../service/book";

import { getImageName } from "../service/users";
import Settings from "./ShowSettings";

export default function Header() {
    const [name, setName] = useState('');
    const [showSettings, setShowSettings] = useState(false);
    const [buttonIcon, setButtonIcon] = useState('˅');
    const [img, setImg] = useState();

    const [cookies, , removeCookie ] = useCookies(['token']);

    const navigate = useNavigate();

    useEffect(()=>{
        async function fetchImages () {
                const imgName = await getImageName();
                if (imgName[0] !== '') {
                    const imgUrl = await fetchImage(imgName);
                    setImg(imgUrl);
                } else {
                    setImg("/userlogo.png");
                }
                // setImg("/userlogo.png");
            }

        fetchImages();
    },[cookies]);

    useEffect(() => {
        setName(cookies.name);
        setShowSettings(false);
    }, [cookies.token, cookies.name]);

    useEffect(()=>{
        if (showSettings){
            setButtonIcon('˄');
        } else {
            setButtonIcon('˅');
        }
    },[showSettings]);

    const handleLogOut = () => {
        removeCookie("token", { path: '/' });
        removeCookie("name", { path: '/' });
        removeCookie("isAdmin", { path: '/' });
        navigate('/authentication/login', { replace: true });
    };

    const handleLogIn = () => {
        navigate('/authentication/login', { replace: true });
    }

    return (
        <div className="header">
            <div className="logo">
                <img src={"/logo.png"} alt="logo" />
            </div>
            <div className="search"></div>
            <div className="userinfo">
                {(cookies?.token && (<>
                    <Settings show={showSettings}/>
                    <span className="username">
                        {name}
                    </span>
                    <span className="userlogo">
                        <img src={img} alt="user" />
                    </span>
                    <span className="settings">
                        <button onClick={()=>{setShowSettings(!showSettings)}}>{buttonIcon}</button>
                    </span>
                    <span className="log">
                        <button onClick={handleLogOut}>Log Out</button>
                    </span>
                </> ))
                || 
                (<span className="log">
                    <button onClick={handleLogIn}>Log In</button>
                </span>)}
            </div>
        </div>
    );
}