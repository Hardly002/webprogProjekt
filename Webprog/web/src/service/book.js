import { apiBase, base } from '../util/constants';
import axios from 'axios';

export async function fetchAllBooks() {
  const response = await fetch(`${apiBase}/books`);
  const books = await response.json();
  return books;
}

export async function deleteBookById(bookId) {
  const resp = await axios.delete(`${apiBase}/books/${bookId}`, {withCredentials: true});
  return [resp.status, resp.data];
}

export async function fetchImage(fileName) {
  const response = await fetch(`${base}/${fileName}`);
  const imageBlob = await response.blob();
  const imageObjectURL = URL.createObjectURL(imageBlob);
  return imageObjectURL;
}

export async function fetchAddBook(formData) {
  const resp = await axios.post(`${apiBase}/books/addbooks`, formData, {withCredentials: true});
  return [resp.status, resp.data];
}

export async function fetchRentBook(formData) {
  const resp = await axios.post(`${apiBase}/books/rentbooks`, formData, {withCredentials: true});
  return [resp.status, resp.data];
}

export async function fetchReturnBook(formData) {
  const resp = await axios.post(`${apiBase}/books/returnbooks`, formData, {withCredentials: true});
  return [resp.status, resp.data];
}

export async function fetchRentsOfBook(bookId) {
  const resp = await axios.get(`${apiBase}/books/rentsofbooks/${bookId}`, {withCredentials: true});
  const list = resp.data.map((data)=> data.Name);
  return list;
}

export async function fetchDescription(bookId) {
  const resp = await axios.get(`${apiBase}/books/${bookId}/description`);
  return resp.data;
}

