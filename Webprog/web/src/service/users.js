import { apiBase } from '../util/constants';
import axios from 'axios';

export async function fetchLogin(formData) {
  const resp = await axios.post(`${apiBase}/authentication/login`, formData, {withCredentials: true});
  return [resp.status, resp.data];
}

export async function fetchRegist(formData) {
  const resp = await axios.post(`${apiBase}/authentication/regist`, formData);
  return [resp.status, resp.data];
}

export async function fetchMyRents() {
  const resp = await axios.get(`${apiBase}/users/myrents`, {withCredentials: true});
  const list = resp.data.map((data)=> data.name);
  return list;
}

export async function fetchAllUsers() {
  const resp = await axios.get(`${apiBase}/users`, {withCredentials: true});
  return resp.data;
}

export async function deleteUserById(userID) {
  const resp = await axios.delete(`${apiBase}/users/${userID}`, {withCredentials: true});
  return [resp.status, resp.data];
}

export async function changeRole(formData) {
  const resp = await axios.post(`${apiBase}/users/changeroles`, formData, {withCredentials: true},);
  return [resp.status, resp.data];
}

export async function fetchUserRents(userID) {
  const resp = await axios.get(`${apiBase}/users/${userID}/rents`, { withCredentials: true });
  const list = resp.data.map((data)=> data.name);
  return list;
}

export async function fetchChangePassword(formData) {
  const resp = await axios.post(`${apiBase}/users/changepassword`, formData, { withCredentials: true });
  return [resp.status, resp.data];
}

export async function getImageName() {
  const response = await axios.get(`${apiBase}/users/picture`, { withCredentials: true });
  return [response.data];
}

export async function changeProfilePicture(formData) {
  const resp = await axios.post(`${apiBase}/users/picture`, formData, { withCredentials: true });
  return [resp.status, resp.data];
}